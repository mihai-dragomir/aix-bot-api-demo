import {
  TransactionRepo,
  TransactionsSessionChainRepo
} from '../../repositories'

import { TRANSACTION_STATUSES } from '../../constants'

const countEntireChainOffersReceived = (transactionsSession, chain) => {
  return TransactionRepo.count({
    transactionsSessionId: { $in: chain.getTransactionsSessionIds() },
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  })
}

const countEntireChainPossibleOffers = (transactionsSession, chain) => {
  return TransactionsSessionChainRepo
    .findTransactionsSessionsOfChainWithId(chain.getId(), { _id: 1 })
    .then(sessions => {
      let ids = sessions.map(s => s.getId())

      return TransactionRepo.countTransactionsOfSessionsWithIds(ids)
    })
}

const countTransactionsSessionOffersReceived = transactionsSession => {
  return TransactionRepo.count({
    transactionsSessionId: transactionsSession.getId(),
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  })
}

const countEntireChainConfirmationsAsked = (transactionsSession, chain) => {
  return TransactionRepo.count({
    transactionsSessionId: { $in: chain.getTransactionsSessionIds() },
    status: TRANSACTION_STATUSES.CONFIRMATION_ASKED
  })
}

const countEntireChainTradeOptionAnswered = (transactionsSession, chain) => {
  return TransactionRepo.count({
    transactionsSessionId: { $in: chain.getTransactionsSessionIds() },
    status: {
      $in: [
        TRANSACTION_STATUSES.ACCEPTED,
        TRANSACTION_STATUSES.DECLINED
      ]
    }
  })
}

export default {
  countEntireChainOffersReceived,
  countEntireChainPossibleOffers,
  countTransactionsSessionOffersReceived,
  countEntireChainConfirmationsAsked,
  countEntireChainTradeOptionAnswered
}
