import {
  TransactionRepo,
  TransactionsSessionRepo,
  TransactionsSessionChainRepo,
  FinancialInstrumentRepo
} from '../../repositories'

export const findAllDependenciesOfTransactionWithId = transactionId => {
  let transaction, transactionsSession, chain, financialInstrument

  return TransactionRepo
    .findById(transactionId)
    .then(transactionEntity => {
      transaction = transactionEntity

      return TransactionsSessionRepo
        .findById(transaction.getTransactionsSessionId())
    })
    .then(transactionsSessionEntity => {
      transactionsSession = transactionsSessionEntity

      return TransactionsSessionChainRepo
        .findById(transactionsSession.getChainId())
    })
    .then(chainEntity => {
      chain = chainEntity

      return FinancialInstrumentRepo.findById(chain.getFinancialInstrumentId())
    })
    .then(financialInstrumentEntity => {
      financialInstrument = financialInstrumentEntity

      return Promise.resolve({
        chain,
        transactionsSession,
        transaction,
        financialInstrument
      })
    })
    .catch(err => {
      console.log('Error in findAllDependenciesOfTransactionWithId, ERR:', err)
    })
}

export default {
  findAllDependenciesOfTransactionWithId
}
