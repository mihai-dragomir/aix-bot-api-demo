import { TransactionsSession } from '../../api/transactions-session'
import TransactionRepo from '../transaction'
import { TRANSACTION_STATUSES } from '../../constants'

const countAllTransactions = transactionsSession => {
  return transactionsSession.transactions.length
}

const find = (matchCondition, projection) => {
  return TransactionsSession.find(matchCondition, projection)
}

const findById = (transactionsSessionId, projection) => {
  return TransactionsSession.findById(transactionsSessionId, projection)
}

const findByIds = (transactionsSessionIds, projection = null) => {
  return TransactionsSession.find(
    { _id: { $in: transactionsSessionIds } },
    projection
  )
}

const findSessionBidsInAscOrder = id => {
  return TransactionRepo.findTransactionsSortedDescByBidValue(
    {
      transactionsSessionId: id,
      bidValue: { $gte: 0 },
      status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
    },
    { _id: 1, bidValue: 1 }
  )
    .catch(err => {
      console.log('findSessionBidsInAscOrder() query failed, ERR:', err)

      return Promise.resolve([])
    })
}

const findSessionOffersInDescOrder = id => {
  return TransactionRepo.findTransactionsSortedAscByOfferValue(
    {
      transactionsSessionId: id,
      bidValue: { $gte: 0 },
      status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
    },
    { _id: 1, offerValue: 1 }
  )
    .catch(err => {
      console.log('findSessionOffersInDescOrder() query failed, ERR:', err)

      return Promise.resolve([])
    })
}

const updateById = (transactionsSessionId, updatedFields = null) => {
  return TransactionsSession.update(
    { _id: transactionsSessionId },
    updatedFields
  ).exec()
}

const updateByIds = (ids, updatedFields = null) => {
  return TransactionsSession.update(
    { _id: { $in: ids } },
    updatedFields,
    { multi: true }
  ).exec()
}

export default {
  find,
  findById,
  findByIds,
  findSessionBidsInAscOrder,
  findSessionOffersInDescOrder,

  countAllTransactions,

  updateById,
  updateByIds
}
