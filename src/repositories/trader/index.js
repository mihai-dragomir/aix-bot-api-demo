import { Trader } from '../../api/trader'

export const findById = (traderId, projection = null) => {
  return Trader.findById(traderId, projection)
}

export const findByIds = (traderIds, projection = null) => {
  return Trader.find({ _id: { $in: traderIds } }, projection)
}

export default {
  findById,
  findByIds
}
