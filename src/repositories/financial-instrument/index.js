import { FinancialInstrument } from '../../api/financial-instrument'

export const findById = (id, projection = null) => {
  return FinancialInstrument.findById(id, projection)
}

export const findAll = (matchCondition = {}, projection = null) => {
  return FinancialInstrument.find(matchCondition, projection)
}

export const findAllCrypto = (projection = null) => {
  return findAll({ category: 'crypto' }, projection)
}

export default {
  findById,
  findAll,
  findAllCrypto
}
