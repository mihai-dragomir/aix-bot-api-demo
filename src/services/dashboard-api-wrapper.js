/**
 * Dedicated script which wraps (abstracts) all the calls to dashboard api.
 */
import request from 'request-promise'
import { dashboardApiRoot } from '../config'

/**
 * Makes a POST request to the dashboard API notifying it that something changed
 * related to transaction statuses.
 */
const notifyDashboardTransactions = () => {
  request.post({
    method: 'POST',
    uri: `${dashboardApiRoot}/transactions-sessions/update-dashboard`,
    json: true
  })
}

export default {
  notifyDashboardTransactions
}
