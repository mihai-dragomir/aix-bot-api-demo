/**
 * The purpose of this script is to define and export all comparator functions
 * used in /services.
 */

// comparator used for comparing objectIds
const objectId = (objectId1, objectId2) => {
  return objectId1.equals(objectId2)
}

export default {
  objectId
}
