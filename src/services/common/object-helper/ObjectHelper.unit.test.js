import ObjectHelper from './ObjectHelper'

describe('ObjectHelper', () => {
  test('should instantiate without error', () => {
    expect(new ObjectHelper()).toMatchObject({})
  })

  let helper = new ObjectHelper()

  describe('flatten() function', () => {
    let fn = helper.flatten

    test('should not throw error', () => {
      expect(() => fn({})).not.toThrow()
      expect(() => fn()).not.toThrow()
      expect(() => fn(null)).not.toThrow()
      expect(() => fn(1)).not.toThrow()
      expect(() => fn({ a: { a: null } })).not.toThrow()
    })

    test('should return expected value', () => {
      expect(fn({})).toEqual({})
      expect(fn({ a: 1 })).toEqual({ a: 1 })
      expect(fn({ a: { b: 1 } })).toEqual({ 'a.b': 1 })
      expect(fn({ a: { b: null } })).toEqual({ 'a.b': null })
      expect(fn({ a: { b: null }, c: true })).toEqual({ 'a.b': null, c: true })
      expect(fn({ a: { b: undefined }, c: 1 }))
        .toEqual({ 'a.b': undefined, c: 1 })
      expect(fn({ a: { a: { a: 'a' } } })).toEqual({ 'a.a.a': 'a' })
    })
  })
})
