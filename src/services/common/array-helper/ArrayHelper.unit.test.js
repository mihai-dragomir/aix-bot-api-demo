import ArrayHelper from './ArrayHelper'
import errors from '../errors'

describe('ArrayHelper', () => {
  test('should instantiate without error', () => {
    expect(new ArrayHelper()).toMatchObject({})
  })

  let helper = new ArrayHelper()

  describe('extractAllMax() function', () => {
    let arr = [
      { _id: 1, x: 1 },
      { _id: 1, x: 2 },
      { _id: 1, x: 2 },
      { _id: 1, x: -20 }
    ]

    test('should throw error', () => {
      expect(() => helper.extractAllMax(arr))
        .toThrowError(errors.IterateeNotAFunctionError)
    })

    test('should not throw error', () => {
      expect(() => helper.extractAllMax(arr, () => {})).not.toThrow()
      expect(() => helper.extractAllMax(1, () => {})).not.toThrow()
      expect(() => helper.extractAllMax('test', () => {})).not.toThrow()
    })

    test('should filter elements correctly', () => {
      let result = helper.extractAllMax(arr, e => e.x)

      expect(result.length).toBe(2)
      result.forEach(e => expect(e.x).toBe(2))

      expect(helper.extractAllMax(2, e => e.x))
        .toEqual(expect.arrayContaining([]))
      expect(helper.extractAllMax(null, e => e.x))
        .toEqual(expect.arrayContaining([]))
      expect(helper.extractAllMax('test', e => e.x))
        .toEqual(expect.arrayContaining([]))
    })
  })

  describe('extractAllMin() function', () => {
    let arr = [
      { _id: 1, x: 1 },
      { _id: 1, x: 1 },
      { _id: 1, x: 2 },
      { _id: 1, x: 20 }
    ]

    test('should throw error', () => {
      expect(() => helper.extractAllMin(arr))
        .toThrowError(errors.IterateeNotAFunctionError)
    })

    test('should not throw error', () => {
      expect(() => helper.extractAllMin(arr, () => {})).not.toThrow()
      expect(() => helper.extractAllMin(1, () => {})).not.toThrow()
      expect(() => helper.extractAllMin('test', () => {})).not.toThrow()
    })

    test('should filter elements correctly', () => {
      let result = helper.extractAllMin(arr, e => e.x)

      expect(result.length).toBe(2)
      result.forEach(e => expect(e.x).toBe(1))

      expect(helper.extractAllMin(2, e => e.x))
        .toEqual(expect.arrayContaining([]))
      expect(helper.extractAllMin(null, e => e.x))
        .toEqual(expect.arrayContaining([]))
      expect(helper.extractAllMin('test', e => e.x))
        .toEqual(expect.arrayContaining([]))
    })
  })

  describe('removeIndexesFromArray() function', () => {
    let arr = [{val: 1}, {val: 2}, {val: 3}, {val: 4}, {val: 5}]
    let fn = helper.removeIndexesFromArray

    test('should return an array', () => {
      expect(Array.isArray(fn([0], arr))).toBeTruthy()
      expect(Array.isArray(fn([], arr))).toBeTruthy()
      expect(Array.isArray(fn(null, arr))).toBeTruthy()
      expect(Array.isArray(fn(undefined, arr))).toBeTruthy()
      expect(Array.isArray(fn([0, 1, 2, 3, 4, 5], arr))).toBeTruthy()
      expect(Array.isArray(fn([0], []))).toBeTruthy()
      expect(Array.isArray(fn([0], null))).toBeTruthy()
      expect(Array.isArray(fn([0]))).toBeTruthy()
      expect(Array.isArray(fn([0], 1))).toBeTruthy()
      expect(Array.isArray(fn([0], 't'))).toBeTruthy()
      expect(Array.isArray(fn())).toBeTruthy()
    })

    test('should remove the specified indexes', () => {
      let result = fn([0], arr)

      expect(result.length).toBe(arr.length - 1)
      expect(result[0]).toEqual(arr[1])
      expect(result[1]).toEqual(arr[2])
      expect(result[2]).toEqual(arr[3])
      expect(result[3]).toEqual(arr[4])

      result = fn([0, 2, 4], arr)

      expect(result.length).toBe(arr.length - 3)
      expect(result[0]).toEqual(arr[1])
      expect(result[1]).toEqual(arr[3])

      result = fn([0, 1, 2, 3, 4, 10], arr)
      expect(result.length).toBe(0)

      result = fn([1000], arr)
      expect(result.length).toBe(arr.length)
    })
  })
})
