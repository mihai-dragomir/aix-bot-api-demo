import BestSpreadCalculator from './BestSpreadCalculator'
import fixturesLoader from '../../../fixtures/fixtures-loader'
import { TransactionsSessionChain } from
  '../../../api/transactions-session-chain'
import errors from '../../common/errors'

describe('BestSpreadCalculator', () => {
  let inst = null
  let chain = null

  test('instantiation should work properly', () => {
    expect(new BestSpreadCalculator()).toMatchObject({})
  })

  beforeEach(() => { inst = new BestSpreadCalculator() })

  describe('formatResult() function', () => {
    test('should work in edge case', () => {
      expect(inst.formatResult({
        highestBids: [],
        lowestOffers: []
      })).toMatchObject({
        highestBid: undefined,
        highestBidTransactionIds: [],
        lowestOffer: undefined,
        lowestOfferTransactionIds: []
      })
    })

    test('shoud throw InvalidBestSpreadFormatResultInputError', () => {
      expect(() => inst.formatResult({}))
        .toThrow(errors.InvalidBestSpreadFormatResultInputError)
      expect(() => inst.formatResult({highestBids: []}))
        .toThrow(errors.InvalidBestSpreadFormatResultInputError)
      expect(() => inst.formatResult({lowestOffers: []}))
        .toThrow(errors.InvalidBestSpreadFormatResultInputError)
    })
  })

  describe('findChainBestSpreadTransactions() function', () => {
    beforeEach(done => {
      fixturesLoader.loadFixtures(true, () => {
        TransactionsSessionChain
          .findOne()
          .then(chainEntity => {
            chain = chainEntity
            done()
          })
          .catch(err => {
            console.log(err)
            done(err)
          })
      })

      test('should work as expected', done => {
        expect(() => int.findChainBestSpreadTransactions('123')).toThrow()
        expect(() =>
          inst
            .findChainBestSpreadTransactions(chain)
            .then(done)
            .catch(done)
        ).not.toThrow()
      })

      test('result should be of expected format', done => {
        inst
          .findChainBestSpreadTransactions(chain)
          .then(result => {
            expect(result).toMatchObject({
              highestBid: expect.any(Number),
              highestBidTransactionIds: expect.any(Array),
              lowestOffer: expect.any(Number),
              lowestOfferTransactionIds: expect.any(Array)
            })

            expect(result.highestBid).toBe(10)
            expect(result.lowestOffer).toBe(12)
            expect(result.highestBidTransactionIds.length).toBe(4)
            result.highestBidTransactionIds.forEach(e => expect(e).toBeTruthy())
            expect(result.lowestOfferTransactionIds.length).toBe(3)
            result.lowestOfferTransactionIds.forEach(e => expect(e).toBeTruthy())

            done()
          })
          .catch(done)
      })
    })
  })
})
