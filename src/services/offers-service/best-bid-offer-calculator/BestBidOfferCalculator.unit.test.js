import BestBidOfferCalculator from './BestBidOfferCalculator'
import fixturesLoader from '../../../fixtures/fixtures-loader'
import { TransactionsSession } from '../../../api/transactions-session'

describe('BestBidOfferCalculator', () => {
  let inst = null
  let transactionsSessionId = null

  test('should instantiate succesfully', () => {
    expect(() => new BestBidOfferCalculator()).not.toThrow()
  })

  beforeEach(done => {
    inst = new BestBidOfferCalculator()
    fixturesLoader.loadFixtures(true, () => {
      TransactionsSession
        .findOne({})
        .then(transactionsSession => {
          transactionsSessionId = transactionsSession._id
          done()
        })
    })
  })

  describe('findSessionTransactionsWithMaxBid() function', () => {
    test(`should work as expected`, done => {
      expect(() => int.findSessionTransactionsWithMaxBid('123')).toThrow()

      expect(() => {
        inst
          .findSessionTransactionsWithMaxBid(transactionsSessionId)
          .then(results => {
            expect(results.length).toBe(4)

            results.forEach(result => expect(result.bidValue).toBe(10))

            done()
          })
      }).not.toThrow()
    })
  })

  describe('findSessionTransactionsWithMinOffer() function', () => {
    test(`should work as expected`, done => {
      expect(() => int.findSessionTransactionsWithMinOffer('123')).toThrow()

      expect(() => {
        inst
          .findSessionTransactionsWithMinOffer(transactionsSessionId)
          .then(results => {
            expect(results.length).toBe(3)

            results.forEach(result => expect(result.offerValue).toBe(12))

            done()
          })
      }).not.toThrow()
    })
  })
})
