import BestSpreadCalculator from
  './best-spread-calculator/BestSpreadCalculator'
import marketPrices from '../market-prices'
import { FinancialInstrumentRepo } from '../../repositories'

/**
 * Class responsible with the generation of the offers to be sent to initiator.
 * @type {Class}
 */
class OffersService extends BestSpreadCalculator {
  /**
   * Creates a class instance and assigns all local functions to its scope.
   * @return {OffersService} A class instance.
   */
  constructor () {
    super()

    this.findChainBestOffers = this.findChainBestOffers.bind(this)
    this.getCurrentMarketPrice = this.getCurrentMarketPrice.bind(this)
  }

  /**
   * Calls its parent to acquire the best chain spread transactions and returns
   * the results together with the bid and offer prices deviation calculated
   * relative to the current market price.
   * @param  {ChainEntity} chain The chain on which to search for the best
   * offers.
   * @return {Object} Object containing the best bid price, best offer price,
   * the transaction ids for the best bids and for the best offers, the
   * bid and offer deviatons relative to the current price.
   */
  findChainBestOffers (chain) {
    return Promise
      .all([
        super.findChainBestSpreadTransactions(chain),
        FinancialInstrumentRepo.findById(chain.getFinancialInstrumentId())
      ])
      .then(([bestSpreadTransactions, financialInstrument]) => {
        const financialInstrumentLabel = financialInstrument.getLabel()
        const currentMarketPrice =
          this.getCurrentMarketPrice(financialInstrumentLabel)
        const { highestBid, lowestOffer } = bestSpreadTransactions

        if (
          typeof highestBid !== 'number' ||
          typeof lowestOffer !== 'number' ||
          typeof currentMarketPrice !== 'number' ||
          currentMarketPrice === 0
        ) {
          return Object.assign(bestSpreadTransactions, {
            bidDeviationFromMarketPrice: Infinity,
            offerDeviationFromMarketPrice: Infinity
          })
        }

        let result = Object.assign(
          bestSpreadTransactions,
          {
            bidDeviationFromMarketPrice: 1 - highestBid / currentMarketPrice,
            offerDeviationFromMarketPrice: lowestOffer / currentMarketPrice - 1
          }
        )

        return result
      })
  }

  /**
   * Gets the current market price.
   * @param  {String} financialInstrumentLabel The identifier of the financial
   * instrument to get the price for.
   * @return {Number} Returns the current market price from the market prices
   * service.
   */
  getCurrentMarketPrice (financialInstrumentLabel) {
    return marketPrices.getCurrentPrice(financialInstrumentLabel)
  }
}

export default OffersService
