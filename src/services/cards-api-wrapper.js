/**
 * Dedicated script which wraps (abstracts) all the calls to cards api.
 */
import request from 'request-promise'

import { cardsApiRoot, masterKey } from '../config'

/**
 * Requests the info cards api to produce a card and returns the promise of it.
 * @param  {String} label   The name of the card type to be generated.
 * @param  {Object} content The content data to be written on the card.
 * @return {Promise}        Promise which resolves depending on the response
 * status.
 */
export const getInfoCardWithContent = (label, content) => {
  return request.post({
    uri: `${cardsApiRoot}/info-cards`,
    headers: {
      Authorization: `Bearer ${masterKey}`
    },
    body: {
      label,
      content
    },
    json: true
  })
}

export default {
  getInfoCardWithContent
}
