/**
 * Configures express routes and middlewares.
 */
import express from 'express'
import forceSSL from 'express-force-ssl'
import cors from 'cors'
import compression from 'compression'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { errorHandler as queryErrorHandler } from 'querymen'
import { errorHandler as bodyErrorHandler } from 'bodymen'
import { env, mongo } from '../../config'

var session = require('express-session')
var MongoDBStore = require('connect-mongodb-session')(session)
global.mongoDbStore = {}

export default (apiRoot, routes) => {
  const app = express()

  mongoDbStore = new MongoDBStore({
    uri: mongo.uri
  })

  // Catch errors
  mongoDbStore.on('error', function (error) {
    console.log(error)
  })

  // Configure the session middleware and register it
  app.use(require('express-session')({
    secret: 'This is a secret',
    cookie: {
      maxAge: 60000 * 10 // 60 min
    },
    store: mongoDbStore,
    // Boilerplate options, see:
    // * https://www.npmjs.com/package/express-session#resave
    // * https://www.npmjs.com/package/express-session#saveuninitialized
    resave: true,
    saveUninitialized: true
  }))

  /* istanbul ignore next */
  if (env === 'production') {
    app.set('forceSSLOptions', {
      enable301Redirects: false,
      trustXFPHeader: true
    })
    app.use(forceSSL)
  }

  /* istanbul ignore next */
  if (env === 'production' || env === 'development') {
    app.use(cors())
    app.use(compression())
    app.use(morgan('dev'))
  }

  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())
  app.use(apiRoot, routes)
  app.use(queryErrorHandler())
  app.use(bodyErrorHandler())

  return app
}
