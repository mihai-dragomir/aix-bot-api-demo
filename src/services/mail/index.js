import pug from 'pug'
import config from '../../config'
const sgMail = require('@sendgrid/mail')

const mailConfig = config.sendgridemail

/**
 * Class specialised on sending email to traders.
 * @type {Class}
 */
class MailService {
  /**
   * Creates a mailService instance.
   */
  constructor () {
    // Compiles the email content template.
    this.transactionConfirmationTemplate = pug.compileFile(
      'src/services/mail/templates/transaction-confirmation.pug'
    )
  }
  /**
   * Generates the mail options required to inform about the transaction
   * approval, and calls local function to send the email.
   * @param  {String} destinationEmail  Receiver email address.
   * @param  {Object} templateVariables The variable values to be replaced in
   * the email html template.
   * @return {undefined}
   */
  sendApprovedTransactionMail (destinationEmail, templateVariables) {
    if (!destinationEmail) {
      console.log('Not sending email because destinationEmail address is not defined.')
    }

    const mailOptions = {
      from: mailConfig.from,
      to: destinationEmail,
      subject: 'AI-Exchange confirmations',
      text: 'This is an email confirmation of your transaction.',
      html: this.transactionConfirmationTemplate(templateVariables)
    }

    this.sendMail(mailOptions, info => {
      console.log('Email Sent! Received info: ', info)
    })
  }

  /**
   * Sends an email to the given destination address, using the given
   * mailOptions and the transporter instance created by the constructor.
   * @param  {Object}   mailOptions The mailOptions to be used.
   * @param  {Function} callback    Function to call after the mail was sent.
   * @return {undefined}
   */
  sendMail (mailOptions, callback) {
    sgMail.setApiKey(mailConfig.apikey)
    sgMail.send(mailOptions, (err, info) => {
      if (err) {
        console.log('Error occurred. ' + err.message)
        return
      }

      console.log('Message sent: ', info.messageId)
      callback(info)
    })
  }
}

export default new MailService()
