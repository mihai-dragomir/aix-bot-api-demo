/**
 * Exports two functions which allow to register or deregister all event
 * listeners exported by the ./listeners directory.
 */
import listeners from './listeners'

export default {
  registerAllListeners: () => {
    listeners.forEach(listener => listener.register())
    console.log('Event listeners from the events-system were registered.')

  },
  deregisterAllListeners: () => {
    listeners.forEach(listener => listener.deregister())
    console.log('Event listeners from the events-system were deregistered.')
  }
}
