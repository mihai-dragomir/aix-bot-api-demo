import numeral from 'numeral'
import isEqual from 'lodash/isEqual'
import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import decisionControl from '../../../../decision-control'
import { TransactionsSessionRepo } from '../../../../repositories'
import { FlowControlManager, SmartManager } from '../../../../managers'
import { TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD } from '../../../../constants'
import { arrayHelper } from '../../../common'
import { offersService } from '../../../offers-service'

/**
 * Periodically measure transactions session parameters while the transactions
 * session is running, check them agains trigger hook conditions and trigger
 * the hook events if conditions match. The periodical actions are executed in
 * an interval loop.
 * @type {Class}
 */
class PeriodicalTransactionsSessionCheckListener extends Listener {
  /**
   * Creates class instance; assign the class scope to the local functions;
   * register a listener which should stop and remove the interval loop.
   */
  constructor () {
    super(eventNames.PERIODICAL_TRANSACTIONS_SESSION_CHECK)

    this.listenerFn = this.listenerFn.bind(this)
    this.registerIntervalForLaterRemove =
      this.registerIntervalForLaterRemove.bind(this)

    /* Register a listener which should stop and remove the referenced interval
    loop. Explanation: The NodeJS interval loop returns interval object (with
    cyclic references) not interval id, when it is created. Because if this, all
    running interval object references are kept inside this class scope and are
    handled by local class functions with access to the class scope. */
    this.runningIntervals = {}
    events.addListener(eventNames.REMOVE_INTERVAL_LOOP, ({ intervalRef }) => {
      let intervalObject = this.runningIntervals[intervalRef]

      console.log('=== Executing REMOVE_INTERVAL_LOOP', intervalRef)
      clearInterval(intervalObject)
      delete this.runningIntervals[intervalRef]
    })
  }

  /**
   * Callback function to execute when event is captured.
   * @param  {TransactionsSessionDocument} transactionsSession The transactions
   *  session for which the checks are running.
   */
  listenerFn ({ transactionsSession }) {
    console.log('Executing PERIODICAL_TRANSACTIONS_SESSION_CHECK listener')
    let chain = null
    let interval = null
    let previousHookTriggerFnParams = null
    let financialInstrument = null

    // save the interval object reference
    interval = setInterval(() => {
      console.log('Checking transactionsSession status')
      console.log(
        'Session target trader types:',
        transactionsSession.targetTraderTypes
      )

      // acquire transactions session related documents to work with
      SmartManager
        .findAsManyDependenciesOf(
          { transactionsSessionId: transactionsSession.getId() }
        )
        .then(dependencies => {
          transactionsSession = dependencies.transactionsSession
          chain = dependencies.chain
          financialInstrument = dependencies.financialInstrument

          if (!transactionsSession) {
            throw new Error('transactionsSession is null')
          }
          if (!chain) {
            throw new Error('chain is null')
          }
          if (!financialInstrument) {
            throw new Error('financialInstrument is null')
          }

          // Use offers service to detect the chain best spread ad the moment.
          return offersService.findChainBestOffers(chain)
        })
        .then(chainBestOffers => {
          // save the spread/offers to the chain
          chain.setCurrentBestOffers(chainBestOffers)

          return chain.save()
        })
        .then(updatedChainEntity => {
          chain = updatedChainEntity // keep the reference to the updated chain

          return FlowControlManager
            .findAllFlowControlParameters(transactionsSession, chain)
        })
        .then(({
          chainReceivedOffersCount,
          chainPossibleOffersCount,
          transactionsSessionReceivedOffersCount,
          repliedAnswersCount,
          confirmationsAskedCount
        }) => {
          /* Constructs a list of call parameters for the hook trigger functions
          . The hooks for which the trigger function returns yes, get executed.
          */
          let hooks = transactionsSession.getHooks()
          let activeTransactionsSessionId = chain.getActiveTransactionsSessionId()
          let transactionsSessionIds = chain.getTransactionsSessionIds()
          let isLastTransactionsSession = transactionsSessionIds
            .indexOf(activeTransactionsSessionId) === transactionsSessionIds.length - 1

          let hookTriggerFnParams = {
            entireChainOffersNo: chainReceivedOffersCount,
            entireChainPossibleOffersNo: chainPossibleOffersCount,
            transactionsSessionOffersNo: transactionsSessionReceivedOffersCount,
            transactionsSessionPossibleOffersNo:
              TransactionsSessionRepo.countAllTransactions(transactionsSession),
            duration: new Date() - transactionsSession.getStartedAt(),
            maxDuration: transactionsSession.getExpiresAt() -
              transactionsSession.getStartedAt(),
            entireChainConfirmationsAskedNo: confirmationsAskedCount,
            entireChainTradeRequestAnswersNo: repliedAnswersCount,
            transactionsSessionStatus: transactionsSession.getStatus(),
            timePassedSinceFirstOffer: chain.firstOfferSentAt ? new Date() -
              chain.firstOfferSentAt : 0,
            tradeActionWasExpressed: chain.wasTradeActionExpresses() || false,
            isIcebergTrade: chain.isIcebergTrade(),
            availableShardsNo: chain.getAvailableShards(),
            totalShardsNo: chain.getTotalShards(),
            isLastTransactionsSession: isLastTransactionsSession,
            chainStatus: chain.getStatus(),
            tradeVolumeLeft: chain.getTradeVolumeLeft(),
            shardVolume: chain.getShardVolume(),
            priceToleranceLimitExists: !!chain.getPriceToleranceLimit(),
            priceToleranceLimit: chain.getPriceToleranceLimit(),
            numberOfOffersSentToInitiator: chain.getNumberOfOffersSentToInitiator(),
            isTradeOptionSpecified: chain.isTradeOptionSpecified(),
            chainBestOffers: chain.getCurrentBestOffers(),
            chainPreviousBestOffers: chain.getPreviousBestOffers()
          }
          // this object is passed as event payload if case a hook is executed
          let hookEmittedEventParams = {
            transactionsSessionId: transactionsSession.getId(),
            chainId: chain.getId(),
            periodicCheckInterval: interval
          }
          let deleteHookAtIndexes = []

          // check each hook
          hooks.forEach((hook, index) => {
            let triggerFn = decisionControl[hook.triggerFn]
            // acquire trigger function response
            let shouldTrigger = triggerFn(hookTriggerFnParams)

            // execute hook if response is true
            if (shouldTrigger) {
              events.emit(
                hook.emitEvent,
                {...hookEmittedEventParams, hookConfig: hook}
              )

              if (hook.singleUse) {
                // of hook is for single use, mark it for deletion
                deleteHookAtIndexes.push(index)
              }
            }
          })

          // remove the executed single use hooks
          if (deleteHookAtIndexes.length) {
            hooks = arrayHelper.removeIndexesFromArray(
              deleteHookAtIndexes,
              hooks
            )
            transactionsSession.setHooks(hooks)
            transactionsSession.save()
          }

          // the following code is just for better debugging the periodic check loop
          let elapsedTimePerc = hookTriggerFnParams.duration /
            hookTriggerFnParams.maxDuration
          let elapsedTimePercString = numeral(elapsedTimePerc).format('0.00%')
          console.log('Elapsed time perc:', elapsedTimePercString)

          if (!isEqual(previousHookTriggerFnParams, hookTriggerFnParams)) {
            console.log('hookTriggerFnParams:', hookTriggerFnParams)
          }
        })
        .catch(err => {
          console.log('Periodical transactions check failed with ERR:', err)
          // TODO do something about this
        })
    }, TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD)

    // register the loop object reference to the runningIntervals collection
    this.registerIntervalForLaterRemove(interval, transactionsSession)
  }

  registerIntervalForLaterRemove (interval, transactionsSession) {
    // generate a fieldname reference for this interval
    let periodicCheckIntervalRef = 'timeout-' + Math.random() * 10
    // add interval to intervals collection
    this.runningIntervals[periodicCheckIntervalRef] = interval
    /* Add the reference to the transactions session document (this will allow
    to remove the interval from outside this class scope). */
    transactionsSession.setPeriodicCheckIntervalRef(periodicCheckIntervalRef)
    transactionsSession.save()
  };
}

export default PeriodicalTransactionsSessionCheckListener
