import async from 'async'

import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import { Transaction } from '../../../../api/transaction'
import { TransactionsSession } from '../../../../api/transactions-session'
import { TransactionsSessionChainRepo } from '../../../../repositories'
import marketMakersDetector from '../../../common/maker-makers-detector'
import {
  TRANSACTION_STATUSES,
  TRANSACTIONS_SESSION_STATUSES
} from '../../../../constants'
import messages from '../../../../user-messages'

/**
 * Make all the required changes to start the specified transactions session.
 * @type {Class}
 */
class TransactionsSessionStartListener extends Listener {
  /*
   * Creates class instance; assigns the classes scope to all class functions.
   */
  constructor () {
    super(eventNames.START_TRANSACTIONS_SESSION)

    this.createTransactionsWithMarketMakers =
      this.createTransactionsWithMarketMakers.bind(this)
    this.listenerFn = this.listenerFn.bind(this)
  }

  /*
    Callback to be called when the listened event is captured.
   */
  listenerFn ({
    transactionsSessionId,
    receiverSessionInitializationData,
    financialInstrumentId
  }) {
    console.log('Executing START_TRANSACTIONS_SESSION listener')
    let marketMakerIds = []
    let transactionsSession = null
    let chain = null

    // Find the transactions session document
    TransactionsSession
      .findById(transactionsSessionId)
      .then(transactionsSessionEntity => {
        // save its reference for later use
        transactionsSession = transactionsSessionEntity
        let traderTypes = transactionsSession.getTargetTraderTypes()

        // find the available traders to trade with
        marketMakersDetector.findCurrentlyAvailable({
          financialInstrumentId: financialInstrumentId,
          initiatorTraderId: transactionsSession.initiatorTraderId,
          traderType: traderTypes.length === 1 ? traderTypes[0] : traderTypes
        })
          .then(_marketMakerIds => {
          // save the traders for later use
            marketMakerIds = _marketMakerIds

            // find the chain to which this transactions session belongs
            return TransactionsSessionChainRepo
              .findById(transactionsSession.getChainId())
          })
          .then(chainEntity => {
          // save the chain reference for later use
            chain = chainEntity

            /* Create transaction documents with each available trader
           (aka. market maker) */
            return this.createTransactionsWithMarketMakers(
              chain,
              transactionsSession,
              marketMakerIds
            )
          })
          .then(transactionIds => {
            transactionsSession.setTransactionIds(transactionIds)
            transactionsSession.setStatus(TRANSACTIONS_SESSION_STATUSES.OPEN)
            transactionsSession.setStartedAt(new Date())
            transactionsSession.setExpiresAt(new Date(
              transactionsSession.getActiveTimeMilliseconds() + Date.now()
            ))

            if (transactionIds.length === 0) {
            /* Delete the transactions session from DB if there are no traders
             were found to trade with. */
              TransactionsSession
                .remove({ _id: transactionsSession.getId() })
                .exec()

              // prepare send message and reset context for initiator
              events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
                traderIds: [transactionsSession.getInitiatorId()],
                message: messages.initiator.noAvailableTraders
              })

              // jump to the catch cause from here
              return Promise.reject(new Error('No traders available.'))
            }

            return transactionsSession.save()
          })
          .then(transactionsSession => {
          // save transactionsSessionId on initiator's session
            events.emit(eventNames.OVERWRITE_TRADERS_SESSION, {
              traderIds: [transactionsSession.getInitiatorId()],
              transactionsSessionId: '' + transactionsSession.getId()
            })
            // send rate request messages to receivers
            events.emit(eventNames.SEND_RATE_REQUEST_MESSAGES, {
              marketMakerIds: marketMakerIds,
              chain: chain,
              transactionsSession: transactionsSession,
              financialInstrumentId: financialInstrumentId
            })

            // overwrite receiver contexts to prepare for their answers
            events.emit(eventNames.OVERWRITE_TRADERS_SESSION, {
              traderIds: marketMakerIds,
              ...receiverSessionInitializationData,
              transactionsSessionId: '' + transactionsSession.getId()
            })
            // emit the event to start a periodic check loop
            events.emit(eventNames.PERIODICAL_TRANSACTIONS_SESSION_CHECK, {
              transactionsSession
            })

            return Promise.resolve()
          })
          .catch(err => {
            console.log('START_TRANSACTIONS_SESSION listener, ERR:', err)
          })
      })
  }

  createTransactionsWithMarketMakers (chain, transactionsSession, marketMakerIds) {
    const commonTransactionData = {
      transactionsSessionId: transactionsSession.getId(),
      traderId: chain.getInitiatorId(),
      financialInstrumentId: chain.getFinancialInstrumentId(),
      offerVolume: 0,
      icebergTrade: chain.isIcebergTrade(),
      status: TRANSACTION_STATUSES.OPEN
    }

    return new Promise((resolve, reject) => {
      async.mapLimit(marketMakerIds, 10, (marketMakerId, callback) => {
        let transaction = Object.assign({}, commonTransactionData, {
          marketMakerId
        })

        Transaction
          .create(transaction)
          .then(newTransaction => callback(null, newTransaction.getId()))
          .catch(() => callback())
      },
      (errors, createdTransactionsIds) => resolve(createdTransactionsIds))
    })
  }
}

export default TransactionsSessionStartListener
