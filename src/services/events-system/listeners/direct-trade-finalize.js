import Listener from '../Listener'
import events, { eventNames } from '../event-emitter'
import {
  TransactionRepo
} from '../../../repositories'
import {
  SmartManager,
  TransactionsSessionManager
} from '../../../managers'
import {
  TRADE_ACTIONS,
  TRANSACTION_STATUSES
} from '../../../constants'
import errors from '../../../services/common/errors'
import messages from '../../../user-messages'

/**
 * Responsible for making the necessary changes to finalize a direct trade. A
 * BUY/SELL action.
 * @type {Class}
 */
class DirectTradeFinalizerListener extends Listener {
  constructor () {
    super(eventNames.HANDLE_DIRECT_TRADE_ACTION)
    // map function scopes to the classes scope
    this.listenerFn = this.listenerFn.bind(this)
    this.determineTransactionIds = this.determineTransactionIds.bind(this)
    this.sendConfirmationMessageToReceiver =
      this.sendConfirmationMessageToReceiver.bind(this)
  }

  listenerFn ({ tradeAction, transactionsSessionId, customTransactionId }) {
    let chain = null
    let transactionsSession = null
    let financialInstrument = null

    // find and save the transactions session related documents, to work with
    SmartManager
      .findAsManyDependenciesOf({ transactionsSessionId })
      .then(dependencies => {
        chain = dependencies.chain
        transactionsSession = dependencies.transactionsSession
        financialInstrument = dependencies.financialInstrument

        // do not handle direct trade if nothing left to trade
        if (!chain.isTradeVolumeLeft()) {
          // exit promise chain and do nothing
          throw errors.NoTradeVolumeAvailableError
        }
        let transactionIds = null

        // determine which transactions to finalize
        if (customTransactionId) {
          transactionIds = [customTransactionId]
        } else {
          transactionIds = this.determineTransactionIds(
            tradeAction,
            chain.getCurrentBestOffers()
          )
        }

        return TransactionRepo.findByIds(transactionIds)
      })
      .then(transactions => {
        let savePromises = []

        // Update chain available trade volume and close as many transactions
        // as possible
        transactions.forEach(transaction => {
          let tradeVolumeLeft = chain.getTradeVolumeLeft()

          if (tradeVolumeLeft >= transaction.getOfferVolume()) {
            transaction.setStatus(TRANSACTION_STATUSES.ACCEPTED)
            transaction.setCloseAction(tradeAction)
            chain.setTradeVolumeLeft(
              tradeVolumeLeft - transaction.getOfferVolume()
            )
            if (transaction.isIcebergTrade()) {
              chain.setAvailableShards(chain.getAvailableShards() - 1)

              this.sendConfirmationMessageToReceiver(
                transaction,
                transactionsSession,
                chain,
                financialInstrument
              )

              TransactionsSessionManager
                .excludeTransactionIdsFromTransactionsSession(
                  transactionsSession,
                  [transaction.getId()]
                )
            }
            // gather all transaction save promises
            savePromises.push(transaction.save())
          }
          // else leave the transaction as it is
        })

        savePromises.push(transactionsSession.save())
        savePromises.push(chain.save())

        // announce initiator what he traded and what he as left
        events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
          traderIds: [chain.getInitiatorId()],
          message: messages.initiator.tradeStatusUpdateFn({
            totalVolume: chain.getTotalTradedVolume(),
            remainingVolume: chain.getTradeVolumeLeft(),
            tradedVolume: chain.getTotalTradedVolume() -
              chain.getTradeVolumeLeft(),
            financialInstrument: financialInstrument.getLabel()
          })
        })

        return Promise.all(savePromises)
      })
      .catch(err => {
        console.log('Failed to handle direct trade action, ERR:', err)
      })
  }

  /**
   * Based on the tradeAction, select the transactions which need to be closed,
   * from the currentBestOffers field of the chain.
   * @param  {String} tradeAction       Constant (BUY/SELL)
   * @param  {Object} currentBestOffers Object containing information about the
   * best offers/spread.
   * @return {Array}                   ids of the transactions which need to be
   * closed.
   */
  determineTransactionIds (tradeAction, currentBestOffers) {
    let transactionIds = null

    switch (tradeAction) {
      case TRADE_ACTIONS.BUY:
        transactionIds = currentBestOffers.lowestOfferTransactionIds; break
      case TRADE_ACTIONS.SELL:
        transactionIds = currentBestOffers.highestBidTransactionIds; break
      default: break
    }

    return transactionIds
  }

  sendConfirmationMessageToReceiver (
    transaction,
    transactionsSession,
    chain,
    financialInstrument
  ) {
    let action = null
    let price = null

    // project trade action and price to match the receiver perspective
    if (
      [TRADE_ACTIONS.BUY, TRADE_ACTIONS.BID]
        .includes(transaction.getCloseAction())
    ) {
      action = TRADE_ACTIONS.SELL.toLowerCase()
      price = transaction.getOfferValue()
    } else {
      action = TRADE_ACTIONS.BUY.toLowerCase()
      price = transaction.getBidValue()
    }

    /* Tell the receiver that the transaction was confirmed, prepare his context
    for a new trade. */
    events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
      traderIds: [transaction.getReceiverId()],
      message:
      [
        messages.receiver.offerConfirmedFn({
          action,
          price,
          amount: transaction.getOfferVolume(),
          coin: financialInstrument.getLabel()
        }),
        messages.receiver.freeToStartANewTrade
      ]
    })
  }
}

export default new DirectTradeFinalizerListener()
