import Listener from '../../Listener'
import botmasterAPI from '../../../botmaster-api-wrapper'
import { Trader } from '../../../../api/trader'
import events, { eventNames } from '../../event-emitter'
import dashboardAPI from '../../../dashboard-api-wrapper'
import {
  TransactionsSessionChainRepo,
  TransactionsSessionRepo,
  TransactionRepo
} from '../../../../repositories'
import { TransactionsSessionChainManager } from '../../../../managers'
import {
  BTC_OR_ETH_PRICE_BUTTONS,
  TRANSACTIONS_SESSION_STATUSES,
  TRANSACTION_STATUSES,
  SESSIONS_CHAIN_STATUSES
} from '../../../../constants'
import messages from '../../../../user-messages'

/**
 * Handle the case when the chain active time has expired and it was not yet
 * closed.
 * @type {Class}
 */
class TransactionsSessionChainTimeoutListener extends Listener {
  constructor () {
    super(eventNames.TRANSACTIONS_SESSION_CHAIN_TIMEOUT)
  }

  // The chainId who has reached timeout
  listenerFn ({ chainId }) {
    console.log('Executing TRANSACTIONS_SESSION_CHAIN_TIMEOUT listener')

    dashboardAPI.notifyDashboardTransactions()

    let chain = null

    TransactionsSessionChainRepo
      .findById(chainId)
      .then(chainEntity => {
        chain = chainEntity
        // stops all periodic check loops (eg. for the running transactions session)
        TransactionsSessionChainManager.stopAllPeriodicCheckLoops(chain)

        // closes all transaction sessions with CLOSED_BY_TIMEOUT status
        return TransactionsSessionRepo.updateByIds(
          chain.getTransactionsSessionIds(),
          { overallStatus: TRANSACTIONS_SESSION_STATUSES.CLOSED_BY_TIMEOUT }
        )
      })
      .then(() => {
        // closes all related transactions with CLOSED status
        return TransactionRepo.updateAllTransactionsOfTransactionsSessionWithIds(
          chain.getTransactionsSessionIds(),
          { status: TRANSACTION_STATUSES.CLOSED }
        )
      })
      .then(() => {
        return TransactionsSessionChainRepo.findChainReceiverTraderIds(chainId)
      })
      .then(receiverIds => {
        let initiatorIds = [chain.getInitiatorId()]

        /* Announces the initiator and the receivers that the trade was
        cancelled because time expired. */
        events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
          traderIds: initiatorIds,
          message: messages.initiator.chainTimeoutReached
        })
        events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
          traderIds: receiverIds,
          message: messages.receiver.transactionsSessionExpired
        })

        Trader.findById(initiatorIds).then((trader) => {
          botmasterAPI.sendButtonsToTrader(
            BTC_OR_ETH_PRICE_BUTTONS,
            trader
          )
        })

        // update chain status to finished
        chain.setStatus(SESSIONS_CHAIN_STATUSES.FINISHED)

        return chain.save()
      })
  }
}

export default TransactionsSessionChainTimeoutListener
