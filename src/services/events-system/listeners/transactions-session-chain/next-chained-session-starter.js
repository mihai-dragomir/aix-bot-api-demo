import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import {
  TransactionsSessionRepo,
  TransactionsSessionChainRepo
} from '../../../../repositories'
import { SESSIONS_CHAIN_STATUSES } from '../../../../constants'

/**
 * Makes the modifications necessary to start the next transactions session of
 * the referenced chain.
 * @type {Class}
 */
class NextChainedSessionStarterListener extends Listener {
  constructor () {
    super(eventNames.START_NEXT_CHAINED_SESSION)

    this.listenerFn = this.listenerFn.bind(this)
    this.closeExistingTransactionsSession =
      this.closeExistingTransactionsSession.bind(this)
    this.openNextTransactionsSession =
      this.openNextTransactionsSession.bind(this)
  }

  /*
    Parameters explanation:
      transactionsSessionId - the id of the currently active transactions
       session.
      chainId - the id of the chain for which to start the next transactions
       session.
   */
  listenerFn ({
    transactionsSessionId,
    chainId
  }) {
    console.log('Executing START_NEXT_CHAINED_SESSION listener')
    let chain = null

    // closes all transactions of the active transactions session
    this.closeExistingTransactionsSession(transactionsSessionId)
      .then(transactionsSession => {
        let tsChainId = transactionsSession
          ? transactionsSession.getChainId() : chainId

        // find the chain of the transactions session
        return TransactionsSessionChainRepo.findById(tsChainId)
      })
      .then(chainEntity => {
        chain = chainEntity

        // get next transactions session id
        let index = chain.transactionsSessionIds.indexOf(transactionsSessionId)
        let nextTransactionsSessionId = chain.transactionsSessionIds[index + 1]

        return this.openNextTransactionsSession(chain, nextTransactionsSessionId)
      })
      .then(transactionsSessionId => {
        chain.setActiveTransactionsSessionId(transactionsSessionId)
        chain.setStatus(SESSIONS_CHAIN_STATUSES.RUNNING)

        return chain.save()
      })
      .catch(err => {
        console.log('Failed to start next chained session, ERR:', err)
      })
  }

  closeExistingTransactionsSession (transactionsSessionId) {
    return new Promise((resolve, reject) => {
      if (!transactionsSessionId) {
        return resolve()
      }

      TransactionsSessionRepo.findById(transactionsSessionId)
        .then(transactionsSession => {
          // Stop the periodic check loop
          events.emit(eventNames.REMOVE_INTERVAL_LOOP, {
            intervalRef: transactionsSession.getPeriodicCheckIntervalRef()
          })

          return resolve(transactionsSession)
        })
        .catch(err => {
          console.log('Failed to stop existing transactions session, ERR:', err)
          resolve()
        })
    })
  }

  openNextTransactionsSession (chain, transactionsSessionId) {
    return new Promise((resolve, reject) => {
      if (!transactionsSessionId) {
        let errorMsg = 'Failed to open next chained transactions session, ERR: ' +
          'transactionsSessionId is not defined.'
        console.log(errorMsg)

        return reject(new Error(errorMsg))
      }

      events.emit(eventNames.START_TRANSACTIONS_SESSION, {
        transactionsSessionId,
        financialInstrumentId: chain.financialInstrumentId,
        receiverSessionInitializationData: chain.receiverSessionInitializationData
      })

      return resolve(transactionsSessionId)
    })
  }
}

export default NextChainedSessionStarterListener
