import Listener from '../../Listener'
import { eventNames } from '../../event-emitter'
import { TransactionsSessionChainRepo } from '../../../../repositories'

/**
 * This is responsible for setting the transactions session stalled flag.
 * @type {Class}
 */
class TransactionsSessionChainStalledFlagSetterListener extends Listener {
  constructor () {
    super(eventNames.MARK_CHAIN_AS_STALLED)
  }

  listenerFn ({ chainId }) {
    console.log('Executing MARK_CHAIN_AS_STALLED listener')

    // uupdated the flag on the chain
    TransactionsSessionChainRepo.updateById(
      chainId,
      { stalled: true }
    )
  }
}

export default TransactionsSessionChainStalledFlagSetterListener
