import uniq from 'lodash/uniq'

import Listener from '../Listener'
import events, { eventNames } from '../event-emitter'
import {
  TransactionRepo,
  TransactionsSessionRepo,
  TransactionsSessionChainRepo
} from '../../../repositories'
import {
  SmartManager,
  TransactionsSessionChainManager
} from '../../../managers'
import {
  TRANSACTIONS_SESSION_STATUSES,
  SESSIONS_CHAIN_STATUSES,
  TRANSACTION_STATUSES,
  TRADE_ACTIONS
} from '../../../constants'
import messages from '../../../user-messages'
import dashboardAPI from '../../dashboard-api-wrapper'
import {
  createCompletedTransaction
} from '../../common/completed-transaction-creator'

/**
 * Specialised in making the changes necessary to complete a standard trade.
 * A non iceberg trade with has no more volume for trade.
 * @type {Class}
 */
class StandardTradeFinalizerListener extends Listener {
  constructor () {
    super(eventNames.FINALIZE_STANDARD_TRADE)

    // assign the class scope to local functions
    this.listenerFn = this.listenerFn.bind(this)
    this.handleAcceptedTransaction = this.handleAcceptedTransaction.bind(this)
    this.handleDeclinedTransaction = this.handleDeclinedTransaction.bind(this)
    this.updateTransactionsSessionsStatuses =
      this.updateTransactionsSessionsStatuses.bind(this)
    this.closeActiveTransactions = this.closeActiveTransactions.bind(this)
    this.prepareReceiverMessages = this.prepareReceiverMessages.bind(this)
  }

  listenerFn ({ transactionsSessionId, chainId }) {
    console.log('Executing FINALIZE_STANDARD_TRADE listener')
    let isAcceptedTransaction = false
    let isDeclinedTransaction = false
    let acceptedTransaction = null
    let declinedTransaction = null
    let chain = null
    let transactionsSession = null
    let financialInstrument = null

    SmartManager
      .findAsManyDependenciesOf({ transactionsSessionId })
      .then(dependencies => {
        return Promise.all([
          TransactionRepo.findFirstAcceptedTransactionOfChainWithId(chainId),
          TransactionRepo.findFirstDeclinedTransactionOfChainWithId(chainId),
          Promise.resolve(dependencies)
        ])
      })
      .then(([
        acceptedTransactionEntity,
        declinedTransactionEntity,
        dependencies
      ]) => {
        acceptedTransaction = acceptedTransactionEntity
        declinedTransaction = declinedTransactionEntity
        chain = dependencies.chain
        financialInstrument = dependencies.financialInstrument
        transactionsSession = dependencies.transactionsSession

        TransactionsSessionChainManager.stopAllPeriodicCheckLoops(chain)

        if (acceptedTransaction) {
          isAcceptedTransaction = !!acceptedTransaction

          return this.handleAcceptedTransaction(acceptedTransaction, dependencies)
        }
        if (declinedTransaction) {
          isDeclinedTransaction = declinedTransaction

          return this.handleDeclinedTransaction(declinedTransaction, dependencies)
        }

        return Promise.reject(
          new Error('Neighter acceptedTransaction nor declinedTransaction were found.')
        )
      })
      // send messages to users
      .then(() => TransactionsSessionChainRepo
        .findChainReceiverTraderIds(chain.getId())
      )
      .then(receiverIds => {
        if (!acceptedTransaction && !declinedTransaction) {
          return Promise.resolve()
        }
        let transaction = acceptedTransaction || declinedTransaction
        let thisReceiverId = transaction.getReceiverId() || null
        let otherReceiverIds = receiverIds.filter(id => id !== thisReceiverId)

        let initiatorIds = [chain.getInitiatorId()]
        let allTraderIds = null // eslint-disable-line no-unused-vars
        if (thisReceiverId) {
          allTraderIds = otherReceiverIds.concat(initiatorIds, thisReceiverId)
        } else {
          allTraderIds = otherReceiverIds.concat(initiatorIds)
        }
        allTraderIds = uniq(allTraderIds)

        if (isAcceptedTransaction) {
          createCompletedTransaction(acceptedTransaction, transactionsSession, chain)
            .then(completedTransaction => {
              events.emit(eventNames.SEND_TRANSACTION_CONFIRMATION_EMAIL, {
                completedTransaction
              })

              events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
                traderIds: otherReceiverIds,
                message: messages.receiver.thankForParticipationFn(
                  completedTransaction.getPrice()
                )
              })
              events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
                traderIds: initiatorIds,
                message: messages.initiator.transactionCongratulation
              })
              events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
                traderIds: [thisReceiverId],
                message: this.prepareReceiverMessages(
                  completedTransaction,
                  financialInstrument
                )
              })

              dashboardAPI.notifyDashboardTransactions()
            })
        }
        if (isDeclinedTransaction) {
          events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
            traderIds: otherReceiverIds,
            message: messages.receiver.thankForParticipationFn()
          })
          events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
            traderIds: initiatorIds,
            message: messages.initiator.transactionRefused
          })
        }

        chain.setStatus(SESSIONS_CHAIN_STATUSES.FINISHED)
        chain.setActiveTransactionsSessionId(null)

        return chain.save()
      })
      .catch(err => {
        console.log('Failed to finalize standard trade, ERR:', err)
      })
  }

  handleAcceptedTransaction (
    acceptedTransaction,
    { transactionsSession, chain }
  ) {
    return this.updateTransactionsSessionsStatuses(
      transactionsSession,
      chain,
      TRANSACTIONS_SESSION_STATUSES.CLOSED_BY_CONFIRM
    )
      .then(() => this.closeActiveTransactions(
        chain.getTransactionsSessionIds()
      ))
  };

  handleDeclinedTransaction (
    declinedTransaction,
    { transactionsSession, chain }
  ) {
    return this.updateTransactionsSessionsStatuses(
      transactionsSession,
      chain,
      TRANSACTIONS_SESSION_STATUSES.CLOSED_BY_DECLINE
    )
      .then(() => this.closeActiveTransactions(
        chain.getTransactionsSessionIds()
      ))
  };

  updateTransactionsSessionsStatuses (transactionsSession, chain, status) {
    return TransactionsSessionRepo
      .updateByIds(chain.getTransactionsSessionIds(), {
        overallStatus: TRANSACTIONS_SESSION_STATUSES.CLOSED
      })
      .then(() => {
        transactionsSession.setStatus(status)

        return transactionsSession.save()
      })
  };

  closeActiveTransactions (transactionsSessionIds) {
    return TransactionRepo
      .updateAllActiveTransactionsOfTransactionsSessionWithIds(
        transactionsSessionIds,
        {
          status: TRANSACTION_STATUSES.CLOSED
        }
      )
  };

  prepareReceiverMessages (completedTransaction, financialInstrument) {
    let receiverMessages = [messages.receiver.transactionCongratulation]
    let closeAction = completedTransaction.getCloseAction()
    let isDirectTrade = [TRADE_ACTIONS.BUY, TRADE_ACTIONS.SELL]
      .includes(closeAction)

    if (isDirectTrade) {
      receiverMessages.splice(0, 0, messages.receiver.offerConfirmedFn({
        action: (
          closeAction === TRADE_ACTIONS.BUY
            ? TRADE_ACTIONS.SELL
            : TRADE_ACTIONS.BUY
        ).toLowerCase(),
        amount: completedTransaction.getVolume(),
        price: completedTransaction.getPrice(),
        coin: financialInstrument.getLabel()
      }))
    }

    return receiverMessages
  }
}

export default new StandardTradeFinalizerListener()
