import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import { USER_TYPES } from '../../../../constants'

/**
 * Action listener that resets the needed watson context fields of the
 * referenced traders, in order to be able understand their future messages
 * as traders decouples from the trade flow. It also sends them an informative
 * message.
 * @type {Class}
 */
class NewTradeFlowPrepareListener extends Listener {
  constructor () {
    super(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW)
  }

  listenerFn ({
    sessionId,
    traderIds = [],
    message
  }) {
    console.log('Executing PREPARE_TRADERS_FOR_NEW_FLOW listener')

    if (message) {
      events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
        traderIds,
        message
      })
    }

    events.emit(eventNames.OVERWRITE_TRADERS_SESSION, {
      traderIds,
      watsonContext: 'empty',
      setContext: true,
      initiatorWorkspaceId: null,
      receiverWorkspaceId: null,
      userType: USER_TYPES.TRADER,
      transactionsSessionChainId: null,
      transactionsSessionId: null
    })
  }
}

export default NewTradeFlowPrepareListener
