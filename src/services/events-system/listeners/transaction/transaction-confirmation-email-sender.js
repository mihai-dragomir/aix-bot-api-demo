import moment from 'moment'
import numeral from 'numeral'
import Listener from '../../Listener'
import { eventNames } from '../../event-emitter'
import { TraderRepo, FinancialInstrumentRepo } from '../../../../repositories'
import mail from '../../../mail'
import { TRADE_ACTIONS } from '../../../../constants'
import messages from '../../../../user-messages'

const MONEY_FORMAT = '0,0.00'

/**
 * Sends email to both the initiator and the receiver of a completed
 * transaction informing them that the transaction was approved.
 * @type {Class}
 */
class TransactionConfirmationEmailSender extends Listener {
  /*
    Creates a class instance; saved the event name to listen for; binds all
    local functions to the class scope.
   */
  constructor () {
    super(eventNames.SEND_TRANSACTION_CONFIRMATION_EMAIL)

    this.listenerFn = this.listenerFn.bind(this)
    this.sendMail = this.sendMail.bind(this)
  }

  /**
   * Called when the listened event was captured
   * @param  {Object} completedTransaction CompletedTransaction document
   *  containing all the required email information.
   */
  listenerFn ({ completedTransaction }) {
    console.log('Executing SEND_TRANSACTION_CONFIRMATION_EMAIL listener')

    // Find the initiator, receiver and financial instrument documents.
    Promise.all([
      TraderRepo.findById(completedTransaction.getInitiatorId()),
      TraderRepo.findById(completedTransaction.getReceiverId()),
      FinancialInstrumentRepo.findById(
        completedTransaction.getFinancialInstrumentId()
      )
    ])
      .then(([initiator, receiver, financialInstrument]) => {
        // The Completed transaction close action is always from the point of view of the initiator.
        const buyingOrBiding = [TRADE_ACTIONS.BUY, TRADE_ACTIONS.BID].includes(completedTransaction.getCloseAction())
        const wallets = buyingOrBiding ? initiator.getWallet() : receiver.getWallet()
        const transactionCurrency = financialInstrument.getLabel()
        let walletAddress = ''

        for (let wallet of wallets) {
          if (wallet.getValue('currency') === transactionCurrency) {
            walletAddress = wallet.getValue('address')
          }
        }

        const bankDetails = buyingOrBiding ? receiver.getBankDetails() : initiator.getBankDetails()
        // request email send to initiator
        this.sendMail({
          emailAddress: initiator.getEmail(),
          fullName: `${initiator.getFirstName()} ${initiator.getLastName()}`,
          trader1: buyingOrBiding ? initiator : receiver,
          actionDescription: buyingOrBiding ? 'bought from' : 'sold to',
          trader2: buyingOrBiding ? receiver : initiator,
          completedTransaction,
          financialInstrument,
          walletAddress,
          bankDetails,
          buyingOrBiding
        })
        // request email send to market maker
        this.sendMail({
          emailAddress: receiver.getEmail(),
          fullName: `${receiver.getFirstName()} ${receiver.getLastName()}`,
          trader1: buyingOrBiding ? initiator : receiver,
          actionDescription: buyingOrBiding ? 'sold to' : 'bought from',
          trader2: buyingOrBiding ? receiver : initiator,
          completedTransaction,
          financialInstrument,
          bankDetails,
          walletAddress,
          buyingOrBiding: !buyingOrBiding
        })

        console.log('Transaction confirmtion mail send was requested.')
      })
      .catch(err => {
        console.log('Failed to acquire data for transaction confirmation email,' +
        ' ERR:', err)
      })
  }

  // local helper function for sending the mail
  sendMail ({
    emailAddress,
    fullName,
    trader1,
    actionDescription,
    financialInstrument,
    trader2,
    completedTransaction,
    bankDetails,
    walletAddress,
    buyingOrBiding
  }) {
    let tradedFinancialInstrumentDescription =
      messages.common.financialInstrumentDescriptionFn({
        financialInstrument,
        completedTransaction
      })
    let value = completedTransaction.getPrice() * completedTransaction.getVolume()
    // prepare mail variables to be replaced in the email template
    let mailTemplateVariables = {
      trader1FirstName: trader1.getFirstName(),
      fullName: fullName,
      trader1LastName: trader1.getLastName(),
      actionDescription: actionDescription,
      trader2FirstName: trader2.getFirstName(),
      trader2LastName: trader2.getLastName(),
      amount: numeral(completedTransaction.getVolume()).format(MONEY_FORMAT),
      tradedFinancialInstrumentDescription,
      price: completedTransaction.getPrice(),
      volume: completedTransaction.getVolume(),
      instrumentName: financialInstrument.getName(),
      companyComission: completedTransaction.getPrice() / 100,
      value: numeral(value).format(MONEY_FORMAT),
      contract: completedTransaction.getId(),
      transactionConfirmationTime: moment(completedTransaction.getCreatedAt())
        .format('MMMM Do YYYY, h:mm:ss a'),
      transactionConfirmationMonth: moment(completedTransaction.getCreatedAt())
        .format('MMMM YYYY'),
      commission: numeral(value * 0.0005).format(MONEY_FORMAT),
      walletAddress: walletAddress,
      buyingOrBiding: buyingOrBiding,
      beneficiaryName: bankDetails.beneficiaryName,
      beneficiaryAddress: bankDetails.beneficiaryAddress,
      accountNumber: bankDetails.accountNumber,
      routingNumber: bankDetails.routingNumber,
      bankName: bankDetails.bankName,
      bankAddress: bankDetails.bankAddress
    }

    // request the mail service to replace the values and send the mail
    mail.sendApprovedTransactionMail(emailAddress, mailTemplateVariables)
  };
}

export default TransactionConfirmationEmailSender
