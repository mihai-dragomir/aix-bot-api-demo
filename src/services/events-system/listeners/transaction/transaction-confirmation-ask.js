/* eslint camelcase: 0 */
import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import dashboardAPI from '../../../dashboard-api-wrapper'
import {
  TransactionRepo,
  TransactionsSessionRepo,
  TransactionsSessionChainRepo
} from '../../../../repositories'
import messages from '../../../../user-messages'
import { TRANSACTION_STATUSES, TRADE_ACTIONS } from '../../../../constants'

/**
 * Builds transaction confirmation ask message for the receivers who have sent
 * a two-way quote and for which the initiator has expressed a trade option. The
 * question is sent to the relevant traders, the transaction status and trader
 * sessions get updated.
 * @type {Class}
 */
class TransactionConfirmationAskListener extends Listener {
  /*
    Creates a class instance, specified the event name to listen for.
   */
  constructor () {
    super(eventNames.ASK_FOR_TRANSACTION_CONFIRMATION)
  }

  /*
    The callback to call when the event is captured. Receives an object with
    the following properties:
    transactionsSessionId - the id of the transactions session whose transaction
      receivers must be asked.
    tradeAction - the trade action expressed by the initiator
      Bid/Offer/Buy/Sell.
    financialInstrument - the label of the traded financial instrument.
    customTransactionId - (optional) a specific transaction whose receiver to
      ask to confirmation.
   */
  listenerFn ({
    transactionsSessionId,
    tradeAction,
    financialInstrument,
    customTransactionId
  }) {
    console.log('Executing ASK_FOR_TRANSACTION_CONFIRMATION listener')
    let transactionsSession = null
    // finds the transactions session
    TransactionsSessionRepo
      .findById(transactionsSessionId)
      .then(transactionsSessionEntity => {
        transactionsSession = transactionsSessionEntity

        return TransactionsSessionChainRepo
          .findById(transactionsSession.transactionsSessionChainId)
      })
      // finds the chain
      .then(chainEntity => {
        let chain = chainEntity

        // prepares the common message data
        const messageInfo = {
          financialInstrument,
          action: tradeAction.toLowerCase(),
          requestMonth: transactionsSession.requestMonth,
          requestAction: transactionsSession.requestAction,
          requestValue: transactionsSession.requestValue
        }

        // determine the transactions whose receivers should get the message
        let transactionIds = null

        switch (tradeAction) {
          case TRADE_ACTIONS.BUY:
          case TRADE_ACTIONS.BID:
            transactionIds =
              chain.getCurrentBestOffers().lowestOfferTransactionIds
            break
          case TRADE_ACTIONS.OFFER:
          case TRADE_ACTIONS.SELL:
            transactionIds =
              chain.getCurrentBestOffers().highestBidTransactionIds
            break
        }

        if (customTransactionId) {
          /*  Uses a specific transactionId instead of the precalculated ones,
          if specified. */
          transactionIds = [customTransactionId]
        }

        // defines an array of session updates to do on the transaction receivers
        let receiverWatsonUpdates = {
          sender_fiat: '$'
        }

        // find the selected transaction documents
        TransactionRepo
          .findByIds(transactionIds)
          .then(transactions => {
            let transactionSavePromises = []

            transactions.map(transaction => {
              // change transaction status to reflect that the question was sent
              transaction.setStatus(TRANSACTION_STATUSES.CONFIRMATION_ASKED)
              // save close action to be known that the question was for a BUY/SELL/BID/OFFER
              transaction.setCloseAction(tradeAction)

              // add extend messageInfo with transaction data
              messageInfo.amount = transaction.getOfferVolume()
              if ([TRADE_ACTIONS.BID, TRADE_ACTIONS.OFFER].includes(tradeAction)) {
                messageInfo.originalBid = transaction.getBidValue()
                messageInfo.originalOffer = transaction.getOfferValue()
              }

              // determine the proposed price for the message
              let price = null
              let sender_bid = false
              let sender_offer = false

              switch (tradeAction) {
                case TRADE_ACTIONS.BUY:
                  price = transaction.getOfferValue()
                  sender_bid = price
                  break
                case TRADE_ACTIONS.SELL:
                  price = transaction.getBidValue()
                  sender_offer = price
                  break
                case TRADE_ACTIONS.BID:
                  price = chain.getAskedBid()
                  sender_bid = price
                  break
                case TRADE_ACTIONS.OFFER:
                  price = chain.getAskedOffer()
                  sender_offer = price
              }
              messageInfo.value = price

              receiverWatsonUpdates.sender_bid = sender_bid
              receiverWatsonUpdates.sender_offer = sender_offer

              transactionSavePromises.push(transaction.save())
            })

            return Promise.all(transactionSavePromises)
          })
          .then(transactions => {
            let receiverIds = transactions.map(t => t.getReceiverId())

            // send message to receivers
            events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
              traderIds: receiverIds,
              // compose the message based on the messageInfo accumulated before
              message: messages.receiver.transactionConfirmationQuestionFn(
                messageInfo
              )
            })

            if (Object.keys(receiverWatsonUpdates).length) {
              // if there is something to update, then do it
              events.emit(eventNames.OVERWRITE_TRADERS_SESSION, {
                traderIds: receiverIds,
                watsonContext: receiverWatsonUpdates,
                setContext: true
              })
            }

            // notify dashboard that transactions status has changed
            dashboardAPI.notifyDashboardTransactions()
          })
      })
  }
}

export default TransactionConfirmationAskListener
