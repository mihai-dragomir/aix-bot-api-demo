import { watsonConfig } from '../../../config'

/**
 * Mocks the given watsonUpdate.
 * @param  {String} message         User message from chat.
 * @param  {Object} previousContext Pre-existing Watson context.
 * @param  {Object} watsonUpdate    New watson response, with new context inside.
 * @param  {Object} session         User's db session.
 * @return {Object}                 The watsonUpdate input, mocked.
 */
export const mockWatsonUpdateResponse = (
  message,
  previousContext,
  watsonUpdate,
  session
) => {
  const { context, output } = watsonUpdate

  if (!previousContext) {
    return watsonUpdate
  }

  /* Filter out messages prepended with //. Those are Watson "comments" which
  should not reach the user's chat. */
  if (output.text) {
    if (typeof output.text === 'string') {
      output.text = [output.text]
    }

    output.text = output.text.filter(message => !message.includes('//'))
  }

  // manually detect when to set Iron Ore as financial instrument
  if (
    !context.financial_instrument_symbol &&
    session.initiatorWorkspaceId === watsonConfig.futureCommodity.initiatorWorkspaceId &&
    session.receiverWorkspaceId === watsonConfig.futureCommodity.receiverWorkspaceId
  ) {
    context.financial_instrument_symbol = 'Iron Ore'
  }

  watsonUpdate.context = context
  watsonUpdate.output = output

  console.log('Watson update mocked successfully!')

  return watsonUpdate
}
