import SessionWrapper from './SessionWrapper'

export const sessionWrapper = new SessionWrapper() // export singleton instance

export default {
  sessionWrapper
}
