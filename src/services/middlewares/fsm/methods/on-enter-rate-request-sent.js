/* eslint camelcase: 0 */
import async from 'async'
import { TransactionsSession } from '../../../../api/transactions-session'
import { TransactionsSessionChain } from
  '../../../../api/transactions-session-chain'
import {
  USER_TYPES,
  TRANSACTIONS_SESSION_STATUSES,
  ACTIVE_CHAIN_STATUSES,
  SESSIONS_CHAIN_STATUSES
} from '../../../../constants'
import helper from '../fsm-helper'
import events, { eventNames } from '../../../events-system/event-emitter'

/**
 * Creates the whole chain | transactions-session | transaction structure and
 * sends the rate request message to the involved receivers of this
 * chain/transactions session.
 * @param  {Object} session                        User's session object.
 * @param  {Object} transition                     FSM transition details.
 * @param  {Object} transactionsSessionChainConfig Config object for sending
 * the rate requests.
 */
const onEnterRateRequestSent = (
  { session },
  transition,
  transactionsSessionChainConfig
) => new Promise((resolve, reject) => {
  // if not config specified don't send rate requests, and abort
  if (!transactionsSessionChainConfig) {
    console.log('Transactions session chain config is missing!')

    return reject(new Error('Missing transactions session chain configuration.'))
  }
  let transactionsSessions = []

  // first check if an active chain already exists, and this trader is initiator
  let activeChainQueryParams = {
    initiatorTraderId: session.trader._id,
    status: { $in: ACTIVE_CHAIN_STATUSES }
  }

  TransactionsSessionChain
    .findOne(activeChainQueryParams, { _id: 1 })
    .then(chain => new Promise((resolve, reject) => {
      // continue if no chain
      if (!chain) {
        return resolve()
      }
      // cancel chain before starting another
      events.emit(eventNames.CANCEL_CHAIN, { chainId: chain.getId() })
      setTimeout(() => resolve(), 2000) // wait for the previous chain to close
    }))
    .catch(err => {
      console.log('Error while checking for pre-existing active chain:', err)

      reject(err)
    })
    .then(() => {
      // creates the transactions sessions
      async.eachSeries(
        transactionsSessionChainConfig.chainSequence,
        (transactionsSessionConfig, callback) => {
          createEmptyTransactionsSession(transactionsSessionConfig)
            .then(transactionsSession => {
              transactionsSessions.push(transactionsSession)
              callback()
            })
            .catch(err => {
              console.log('Failed to create transactionsSession, ERR:', err)
              callback()
            })
        },
        errs => {
          let transactionsSessionIds = transactionsSessions.map(ts => ts.getId())
          let isIcebergTrade = helper.isIcebergTradeAllowed(session)
          let shardSize = helper.getShardSize(session)
          let bidVolume = helper.getRequestQuantity(session)

          // creates the chain for the transaction sessions
          TransactionsSessionChain
            .create({})
            .then(emptyChain => {
              session.transactionsSessionChainId = '' + emptyChain.getId()
              Object.assign(emptyChain, {
                transactionsSessionIds: transactionsSessionIds,
                receiverSessionInitializationData:
                  prepareReceiverInitializationData( // for watson context
                    emptyChain.getId(),
                    isIcebergTrade,
                    shardSize
                  ),
                activeTransactionsSessionId: null,
                status: SESSIONS_CHAIN_STATUSES.PENDING,
                initiatorTraderId: '' + session.trader._id,
                financialInstrumentId: session.financialInstrument._id,
                initiatorTraderSessionId: session.id,
                totalTradedVolume: bidVolume,
                tradeVolumeLeft: bidVolume,
                icebergTrade: isIcebergTrade,
                totalShards: isIcebergTrade ? bidVolume / shardSize : null,
                availableShards: isIcebergTrade ? bidVolume / shardSize : null,
                soldShardTransactions: isIcebergTrade ? [] : null,
                shardVolume: isIcebergTrade ? shardSize : null,
                priceToleranceLimit: null
              })

              return emptyChain.save()
            })
            .then(tsChainEntity => {
              // links the created transaction sessions to the chain
              TransactionsSession.update(
                { _id: { $in: transactionsSessionIds } },
                { transactionsSessionChainId: tsChainEntity._id },
                { multi: true }
              ).then(() => {
                // assign new values to watson context
                session.watson.context.market_quantity = helper
                  .getRequestQuantity(session)
                session.watson.context.market_remaining_quantity = helper
                  .getRequestQuantity(session)
                // make this flag true to send /set-context message to Watson
                session.setContext = true

                // start first transactions session of the chain
                events.emit(eventNames.START_NEXT_CHAINED_SESSION, {
                  transactionsSessionId: null,
                  chainId: tsChainEntity.getId()
                })

                resolve()
              })
                .catch(err => {
                  console.log(
                    'Failed to set activeTransactionsChainId on ' +
                  'transaction sessions, ERR', err
                  )
                })
            })
            .catch(err => {
              console.log('Failed to create transactionsSessionChain, ERR:', err)
              reject(err)
            })
        }
      )
    })

  function createEmptyTransactionsSession (config) {
    return TransactionsSession
      .create({
        initiatorTraderSessionId: session.id,
        initiatorTraderId: session.trader._id,
        overallStatus: TRANSACTIONS_SESSION_STATUSES.PENDING,
        bidVolume: helper.getRequestQuantity(session),
        requestValue: helper.getRequestValue(session),
        requestAction: helper.getRequestAction(session),
        requestMonth: helper.getRequestMonth(session),
        requestYear: helper.getRequestYear(session),
        transactions: [],
        targetTraderTypes: config.traderTypes,
        activeTimeMilliseconds: config.activeTime,
        hooks: config.hooks
      })
  };

  function prepareReceiverInitializationData (
    chainId,
    isIcebergTrade,
    shardSize
  ) {
    const {
      request_quantity,
      request_crypto,
      request_month,
      request_year,
      request_value,
      request_action,
      request_iceberg,
      request_shard_size,
      request_crypto_symbol,
      fiat_currency_list,
      _dic,
      crypto_currency_list,
      ...watsonContext
    } = session.watson.context

    // modify some watson context fields to match receiver perspective

    return {
      initiatorWorkspaceId: session.initiatorWorkspaceId,
      receiverWorkspaceId: session.receiverWorkspaceId,
      transactionsSessionChainId: '' + chainId,
      userType: USER_TYPES.MARKET_MAKER,
      watsonContext: {
        sender_quantity: isIcebergTrade ? shardSize : request_quantity,
        sender_crypto: request_crypto,
        sender_crypto_symbol: request_crypto_symbol,
        sender_iceberg: request_iceberg,
        sender_shard_size: request_shard_size,
        fiat_currency_list: fiat_currency_list,
        sender_month: request_month,
        sender_year: request_year,
        sender_value: request_value,
        sender_option: request_action,
        _dic,
        crypto_currency_list
      },
      setContext: true
    }
  }
})

export default onEnterRateRequestSent
