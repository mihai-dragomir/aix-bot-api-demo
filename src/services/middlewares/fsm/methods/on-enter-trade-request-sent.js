import helper from '../fsm-helper'
import { TransactionsSessionChain } from '../../../../api/transactions-session-chain'
import events, { eventNames } from '../../../events-system/event-emitter'
import { TRADE_ACTIONS } from '../../../../constants'

/**
 * After the initiator trader has picked a trade option (BUY/SELL/BID/OFFER).
 * @param  {Object} session User's session.
 */
const onEnterTradeRequestSent = ({ session }) => {
  let tradeAction = null

  if (!helper.isTradeOptionPresent(session)) {
    return Promise.reject(new Error('Trade option was not found on session!'))
  }

  return TransactionsSessionChain
    .findById(session.transactionsSessionChainId)
    .then(chain => {
      // determines the selected trade action
      if (helper.isBuyAction(session)) {
        tradeAction = TRADE_ACTIONS.BUY
      } else if (helper.isSellAction(session)) {
        tradeAction = TRADE_ACTIONS.SELL
      } else if (helper.isBidAction(session)) {
        tradeAction = TRADE_ACTIONS.BID
      } else if (helper.isOfferAction(session)) {
        tradeAction = TRADE_ACTIONS.OFFER
      }

      if (tradeAction === null) {
        return Promise.reject(new Error('No trade action identified!'))
      }

      chain.setTradeActionWasExpressed(true)
      // making this false allows new offer to be displayed to initiator
      chain.setTradeOptionSpecified(false)
      // save trade action on the chain
      chain.setTradeOption(tradeAction)

      // get and save the trade value to the chain
      let tradeValue = helper.getTradeValue(session)

      switch (tradeAction) {
        case TRADE_ACTIONS.BID: chain.setAskedBid(tradeValue); break
        case TRADE_ACTIONS.OFFER: chain.setAskedOffer(tradeValue); break
      }

      // get and save price limit if exists
      if (helper.isIcebergTradeAllowed(session)) {
        let priceToleranceLimit = helper.getPriceToleranceLimit(session)

        if (typeof priceToleranceLimit === 'number') {
          chain.setPriceToleranceLimit(priceToleranceLimit)
        }
      }

      // update chain
      return chain.save()
    })
    .then(chain => {
      // handle trade
      let directTrades = [TRADE_ACTIONS.BUY, TRADE_ACTIONS.SELL]

      if (directTrades.includes(tradeAction)) {
        events.emit(eventNames.HANDLE_DIRECT_TRADE_ACTION, {
          transactionsSessionId: chain.getActiveTransactionsSessionId(),
          tradeAction: tradeAction
        })
      } else {
        events.emit(eventNames.ASK_FOR_TRANSACTION_CONFIRMATION, {
          transactionsSessionId: chain.getActiveTransactionsSessionId(),
          tradeAction: tradeAction,
          financialInstrument: session.financialInstrument.label
        })
      }

      return Promise.resolve()
    })
    .catch(err => {
      console.log('Trade request send failed, ERR:', err)
    })
}

export default onEnterTradeRequestSent
