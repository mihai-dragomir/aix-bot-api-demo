import helper from '../fsm-helper'
import decisionControl from '../../../../decision-control'
import _sendMessageToWatson from '../../../common/send-message-to-watson'
import { watsonConfig } from '../../../../config'
import { mockWatsonUpdateResponse } from '../../watson-communication/mocks'

const watsonDeveloperCloud = require('watson-developer-cloud')

/**
 * Handles the flow control point which decides if this trade should be handled
 * as a iceberg trade or not. (If handled as Iceberg trade, additional questions
 * are sent by Watson during the flow).
 *
 * session: User's session.
 */
const onEnterIcebergTradeDecision = ({ session }) => {
  let requestQuantity = helper.getRequestQuantity(session)

  let askForIceberg = decisionControl
    .shouldAskForIcebergTrade(requestQuantity) // take the decision
  let sendMessage = askForIceberg ? '/yes' : '/no'

  let watson = watsonDeveloperCloud.conversation({
    username: watsonConfig.username,
    password: watsonConfig.password,
    url: watsonConfig.url,
    version: 'v1', // as of this writing (01 Apr 2017), only v1 is available
    version_date: '2017-05-26' // latest version-date as of this writing
  })

  // send the flow decision response
  return _sendMessageToWatson({
    workspaceId: watsonConfig.crypto.initiatorWorkspaceId,
    watson,
    context: session.watson.context,
    text: sendMessage
  })
    .then(watsonUpdate => {
      // mock the updated
      session.watson = mockWatsonUpdateResponse(
        sendMessage,
        session.watson.context,
        watsonUpdate,
        session
      )
      console.log('Watson context after flow control:', watsonUpdate)

      return Promise.resolve()
    })
    .catch(err => {
      console.log('Failed to send message to watson, ERR:', err)
    })
}

export default onEnterIcebergTradeDecision
