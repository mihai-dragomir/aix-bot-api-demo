import moment from 'moment'
import numeral from 'numeral'

import marketPrices from '../../market-prices'
import CardsApi from '../../cards-api-wrapper'
import helper from '../fsm/fsm-helper'

/**
 * Middleware responsible for requesting different card images and sending them
 * to the user's chat.
 *
 * To be mounted on POST /bot-messages endpoint.
 *
 * @param {options} Middleware predefined options (Nothing at the moment).
 * @return {Function} The middleware function which should be mounted.
 */
export const watsonCardsDisplayMiddleware = options => {
  console.log('Watson Cards Display Middleware')

  const watsonCardsDisplayFn = (req, res, next) => {
    const { session } = req

    // don't do this in unit test mode
    if (process.env.NODE_ENV === 'unit_test') {
      return next()
    }

    if (!session.watson || !session.watson.context || session.isWatsonError) {
      return next()
    }

    switch (session.watson.context.display_card) {
      case 'crypto_rates':
        const coin = session.watson.context.request_crypto_symbol
        const currency = 'USD'
        const currencySymbol = '$'
        const priceFormat = '0,0.00'

        if (typeof session.outputMessage === 'string') {
          session.outputMessage = [session.outputMessage]
        }

        let currentPrice = numeral(
          marketPrices.getCurrentPrice(coin, currency)
        ).format(priceFormat)
        let lowestPrice = numeral(
          marketPrices.getTodaysLowestPrice(coin, currency)
        ).format(priceFormat)
        let highestPrice = numeral(
          marketPrices.getTodaysHighestPrice(coin, currency)
        ).format(priceFormat)

        return CardsApi.getInfoCardWithContent('crypto_rates', {
          coin: coin,
          coinQuantity: 1,
          currency: currency,
          currencySymbol: currencySymbol,
          currentPrice: currentPrice,
          dayLowPrice: lowestPrice,
          dayHighPrice: highestPrice,
          now: moment().format('LTS')
        })
          .then(infoCard => {
            console.log('CARD IMAGE url received, sending it to botmaster.')

            let botmasterMessage = JSON.stringify({
              imgUrl: infoCard.url,
              coin: infoCard.content.coin
            })

            session.outputMessage.push(botmasterMessage)

            next()
          })
          .catch(err => {
            console.log('Card generate failed with, ERR', err)

            session.outputMessage = [
              `Here are the ${coin} prices info for today:`,
              `Current price: ${currencySymbol}${currentPrice}`,
              `Lowest price: ${currencySymbol}${lowestPrice}`,
              `Highest price: ${currencySymbol}${highestPrice}`
            ]

            if (!helper.getTradeSize(session)) {
              session.outputMessage.push('What size do you want to trade?')
            }

            next()
          })
      default: next()
    }
  }

  return watsonCardsDisplayFn
}

export default new watsonCardsDisplayMiddleware()
