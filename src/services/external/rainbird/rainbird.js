/**
 * A service no longer used by the project. This code still remains here
 * because it will be used in the future.
 */
var api = require('./yolapi.js')
var RainBird = function () {}

RainBird.prototype.getRainBirdResponse = function (callback) {
  var session = new api.session(
    'https://api.rainbird.ai', // The environment you're targetting.
    '0c0161bc-ec2c-40c0-bd11-5eceebc1fcf4', // Your Api key.
    '0abc6a6d-537d-42a7-ac93-f54f930786b1' // The knowledge-map ID.
  )

  var injectData = [
    {relationship: 'involves', subject: '5a8aad1eea1d244640e3ae40', object: 'the user', cf: 100},
    {relationship: 'objects to', subject: 'the user', object: 'ask price', cf: 100},
    {relationship: 'at position', subject: '5a8a87461835ff3b54743fe5', object: 'obj1', cf: 100},
    {relationship: 'is for', subject: '5a8a87461835ff3b54743fe5', object: 'iron ore', cf: 100},
    {relationship: 'valued', subject: 'ask price', object: 12000, cf: 100},
    {relationship: 'last traded at', subject: 'ask price', object: 12540, cf: 100},
    {relationship: 'queried on', subject: 'iron ore', object: '2018-02-16', cf: 100},
    {relationship: 'has week one date', subject: 'security', object: '2018-02-16', cf: 100},
    {relationship: 'has week two date', subject: 'security', object: '2018-02-09', cf: 100},
    {relationship: 'has week three date', subject: 'security', object: '2018-02-02', cf: 100}

  ]

  var queryData = { relationship: 'should be shown', subject: 'the user', object: null }

  session.start(function (err, result) {
    if (err) console.log(err)
    session.inject(injectData, function (err, result) {
      if (err) console.log(err)
      session.query(queryData, function (err, result) {
        if (err) console.log(err)
        // Write out the response from Rainbird.
        var content = JSON.parse(result)
        callback(content)
      })
    })
  })
}
