import { TRADE_ACTIONS } from '../../constants'
import numeral from 'numeral'

/**
 * This script defines receiver messages or functions that generate messages, to
 * be displayed into the receiver trader's chat.
 */

const invalidOfferMessageFn = ({ spreadString, financialInstrument }) => {
  return `Hello {trader}, your price of ${spreadString} is invalid for ` +
    `${financialInstrument}.`
}

const tradeRefusedNoVolumeAvailableMessage = `Sorry, your trade was rejected ` +
  `because there was not enough volume left to trade.`

const marketMakerRateRequestMessageFn = (
  chain,
  transactionsSession,
  financialInstrument,
  currentMarketPrice
) => {
  let { icebergTrade, shardVolume, totalTradedVolume } = chain
  let {
    requestMonth,
    requestYear,
    requestAction,
    requestValue
  } = transactionsSession

  if (financialInstrument === 'Iron Ore') {
    totalTradedVolume = numeral(totalTradedVolume).format('0.00a').toUpperCase()

    return `Hello {trader}, I'm looking for a two way price in ` +
      `${totalTradedVolume} tonnes of ${requestMonth} ${requestYear} ` +
      `$${requestValue} strike Iron Ore ${requestAction}, where would you be ` +
      `on that please?`
  }

  // if (isIcebergTrade) {
  //   return `Hello trader, I\'m looking for a two way price for ${totalShards}` +
  //     ` shards of ${numeral(shardVolume).format('0.000')} ` +
  //     `${financialInstrument} each. The entire volume for trade is ` +
  //     `${totalTradedVolume} ${financialInstrument}. Type "cancel" if you don't want ` +
  //     `to participate on this trade.`
  // }

  return `Hello {trader}, I'm looking for a two way price in ` +
    `${icebergTrade ? shardVolume : totalTradedVolume} ` +
    `${financialInstrument}/USD, last trading at ${currentMarketPrice} ([Source](cryptocompare.com))`
}

const transactionConfirmationQuestionMessageFn = ({
  action, value, amount, financialInstrument, requestMonth, requestYear,
  requestValue, requestAction, originalBid, originalOffer
}) => {
  if (financialInstrument === 'Iron Ore') {
    amount = numeral(amount).format('0.00a').toUpperCase()

    return `A trader is willing to ${action} ${amount} tonnes of ` +
      `${requestMonth} ${requestYear} $${requestValue} strike Iron Ore ` +
      `${requestAction} ` + (action === 'sell' ? 'to' : 'from') +
      ` you for $${value}. Do you confirm the transaction?`
  }

  // is crypto trade
  if ([TRADE_ACTIONS.BUY, TRADE_ACTIONS.SELL].includes(action.toUpperCase())) {
    return `A trader is willing to ${action} ${amount} ${financialInstrument} ` +
    (action === 'sell' ? 'to' : 'from') + ` you for $${value}. Do you confirm ` +
    `the transaction?`
  }
  if (TRADE_ACTIONS.BID === action.toUpperCase()) {
    return `Hello {trader}, I can show you a bid at $${value} for ${amount} ` +
      `${financialInstrument}, against your $${originalOffer} offer. Do you ` +
      `wish to sell?`
  }
  if (TRADE_ACTIONS.OFFER === action.toUpperCase()) {
    return `Hello {trader}, I can show you an offer at $${value} for ` +
      `${amount} ${financialInstrument}, against your $${originalBid} bid. Do` +
      ` you wish to buy?`
  }
}

const transactionCancelledMessage = 'The trade is no longer ' +
  ' relevant, it was cancelled.'

const transactionsSessionExpiredMessage = 'This trade has expired.'

const thankForParticipationMessageFn = price => {
  if (price) {
    return `This traded at $${price}.`
  }

  return 'The trade is no longer active. Thank you for participation.'
}

const transactionCongratulationMessage = 'You are free to ' +
  'start another trade now.'

const offerConfirmedMessageFn = params => {
  if (!params) {
    return 'Transaction confirmed!'
  }

  let { action, amount, coin, price } = params

  if(action == TRADE_ACTIONS.SELL.toLowerCase()){
    action = 'sold'
  }
  if(action == TRADE_ACTIONS.BUY.toLowerCase()){
    action = 'bought'
  }
  return `That's confirmed, you ${action} ${amount} ${coin} at $${price}. ` +
    `Confirmation has been sent to your email. Thank you for the business.`
}

const offerOutOfToleratedLimitMessage = `This offer doesn't meet the ` +
  `initiator's interests.`

const freeToStartANewTradeMessage = 'You are free to start a new trade.'

const traderRateRequestRefusedMessage = 'Done. You can now take part ' +
  'in another trade.'

export default {
  invalidOfferFn: invalidOfferMessageFn,
  tradeRefusedNoVolumeAvailable: tradeRefusedNoVolumeAvailableMessage,
  marketMakerRateRequestFn: marketMakerRateRequestMessageFn,
  transactionConfirmationQuestionFn: transactionConfirmationQuestionMessageFn,
  transactionCancelled: transactionCancelledMessage,
  transactionsSessionExpired: transactionsSessionExpiredMessage,
  thankForParticipationFn: thankForParticipationMessageFn,
  transactionCongratulation: transactionCongratulationMessage,
  offerConfirmedFn: offerConfirmedMessageFn,
  offerOutOfToleratedLimit: offerOutOfToleratedLimitMessage,
  freeToStartANewTrade: freeToStartANewTradeMessage,
  traderRateRequestRefused: traderRateRequestRefusedMessage
}
