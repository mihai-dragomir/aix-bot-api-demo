const financialInstruments = [
  {
    label: 'BTC',
    name: 'Bitcoin',
    category: 'crypto'
  },
  {
    label: 'ETH',
    name: 'Ethereum',
    category: 'crypto'
  },
  {
    label: 'ETC',
    name: 'Ethereum Classic',
    category: 'crypto'
  },
  {
    label: 'WTI Oil',
    name: 'Crude Oil',
    category: 'future_commodity'
  },
  {
    label: 'Iron Ore',
    name: 'Iron Ore',
    category: 'future_commodity'
  }
]

export default financialInstruments
