export const traderFinancialInstrumentsMap = [
  {
    traderMatchCondition: { telegramId: 497957162 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 549028159 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 433411531 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 509318934 },
    financialInstrumentMatchCondition: { } // all
  }
]

export const transactionToTransactionsSessionMap = [
  {
    transactionMatchCondition: { }, // all
    transactionsSessionMatchCondition: { } // all
  }
]

export const transactionsSessionToChainMap = [
  {
    transactionsSessionMatchCondition: { }, // all
    chainMatchCondition: { } // all
  }
]

export const financialInstrumentToChainMap = [
  {
    financialInstrumentMatchCondition: { }, // all
    chainMatchCondition: { } // all
  }
]

export default {
  traderFinancialInstrumentsMap,
  transactionToTransactionsSessionMap,
  transactionsSessionToChainMap,
  financialInstrumentToChainMap
}
