import { TRADER_TYPES } from '../../constants'

const traders = [
  {
    telegramId: 497957162,
    mobileNumber: '0747775175',
    firstName: 'XY',
    lastName: 'man',
    password: '508068273',
    email: 'o@gmail.com',
    financialInstrumentsInUse: [],
    traderType: TRADER_TYPES.STANDARD
  },
  {
    telegramId: 549028159,
    mobileNumber: '0747705861',
    firstName: 'Johny',
    lastName: 'Test',
    password: '650139260',
    email: 'o@gmail.com',
    financialInstrumentsInUse: [],
    traderType: TRADER_TYPES.STANDARD
  },
  {
    telegramId: 433411531,
    mobileNumber: '0743476594',
    firstName: 'Big',
    lastName: 'Brother',
    password: '544522642',
    email: 'o@gmail.com',
    financialInstrumentsInUse: [],
    traderType: TRADER_TYPES.STANDARD
  },
  {
    telegramId: 509318934,
    mobileNumber: '0752436045',
    firstName: 'Salt',
    lastName: 'And Pepper',
    password: '610429045',
    financialInstrumentsInUse: [],
    email: 'salt_123456@gmail.com',
    traderType: TRADER_TYPES.STANDARD
  }
]

export default traders
