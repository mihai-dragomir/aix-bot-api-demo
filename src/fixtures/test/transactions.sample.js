import { TRANSACTION_STATUSES } from '../../constants'

const transactions = [
  {
    transactionsSessionId: null, // to be defined by map
    offerValue: 12,
    bidValue: 10,
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  },
  {
    transactionsSessionId: null, // to be defined by map
    offerValue: 12,
    bidValue: 10,
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  },
  {
    transactionsSessionId: null, // to be defined by map
    offerValue: 12,
    bidValue: 9,
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  },
  {
    transactionsSessionId: null, // to be defined by map
    offerValue: 13,
    bidValue: 10,
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  },
  {
    transactionsSessionId: null, // to be defined by map
    offerValue: 15,
    bidValue: 5,
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  },
  {
    transactionsSessionId: null, // to be defined by map
    offerValue: 20,
    bidValue: 10,
    status: TRANSACTION_STATUSES.RECEIVER_ANSWERED
  }
]

export default transactions
