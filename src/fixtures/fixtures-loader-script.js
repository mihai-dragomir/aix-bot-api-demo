import mongoose from '../services/mongoose'
import { mongo } from '../config'
import fixturesLoader from './fixtures-loader'

mongoose.connect(mongo.uri, { useMongoClient: true }, err => {
  if (err) {
    throw err
  }
  fixturesLoader.loadFixtures(false, () => {
//    mongoose.connection.close()
  })
})
