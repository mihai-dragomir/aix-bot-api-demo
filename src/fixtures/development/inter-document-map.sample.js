export const traderFinancialInstrumentsMap = [
  {
    traderMatchCondition: { telegramId: 483685448 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 499418762 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 498915883 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 491337682 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 509318934 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 512412033 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 516000462 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 464357131 },
    financialInstrumentMatchCondition: { } // all
  },
  {
    traderMatchCondition: { telegramId: 418116172 },
    financialInstrumentMatchCondition: { } // all
  }
]

export default {
  traderFinancialInstrumentsMap
}
