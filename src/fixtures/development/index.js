import traders from './traders'
import financialInstruments from './financial-instruments'
import maps from './inter-ducument-map'

export default {
  traders,
  financialInstruments,
  maps
}
