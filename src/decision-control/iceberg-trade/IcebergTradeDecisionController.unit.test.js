import IcebergTradeDecisionController from './IcebergTradeDecisionController'
import { SESSIONS_CHAIN_STATUSES } from '../../constants'

describe('IcebergTradeDecisionController', () => {
  let controllerInst = null
  let testedFn = null

  beforeEach(() => {
    controllerInst = new IcebergTradeDecisionController()
    // mock functions
    controllerInst.getMarketAverageTradeVolume = jest.fn()
      .mockImplementation(() => 2)
  })

  describe('instance', () => {
    test('should contain the needed constants', () => {
      expect(controllerInst).toMatchObject({
        AVERAGE_VOLUME_MULTIPLIER: expect.any(Number),
        SESSIONS_CHAIN_STATUSES: expect.any(Object),
        TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD: expect.any(Number)
      })
    })
  })

  describe('shouldAskForIcebergTrade() function', () => {
    test('should answer as expected', () => {
      expect(controllerInst.shouldAskForIcebergTrade(-10)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(0)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(9)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(10)).toBeTruthy()
      expect(controllerInst.shouldAskForIcebergTrade(1000)).toBeTruthy()
      expect(controllerInst.shouldAskForIcebergTrade(null)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade({})).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(true)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(undefined)).toBeFalsy()

      controllerInst.getMarketAverageTradeVolume = jest.fn()
        .mockImplementation(() => undefined)
      expect(controllerInst.shouldAskForIcebergTrade(0)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(100)).toBeFalsy()

      controllerInst.getMarketAverageTradeVolume = jest.fn()
        .mockImplementation(() => null)
      expect(controllerInst.shouldAskForIcebergTrade(0)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(100)).toBeFalsy()

      controllerInst.getMarketAverageTradeVolume = jest.fn()
        .mockImplementation(() => {})
      expect(controllerInst.shouldAskForIcebergTrade(0)).toBeFalsy()
      expect(controllerInst.shouldAskForIcebergTrade(100)).toBeFalsy()
    })
  })

  describe('shouldFinalizeIcebergTrade() function', () => {
    beforeEach(() => {
      testedFn = controllerInst.shouldFinalizeIcebergTrade
      controllerInst.TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD = 2 * 1000
    })

    test('should return the correct decision when not in iceberg mode', () => {
      expect(testedFn({ isIcebergTrade: false })).toBeFalsy()

      generatePeriodicCheckParams(false, 0, 0, 10 * 1000).forEach(params => {
        expect(testedFn(params)).toBeFalsy()
      })
    })

    test('should return the correct decision when in iceberg mode', () => {
      expect(testedFn({ isIcebergTrade: false })).toBeFalsy()

      generatePeriodicCheckParams(false, 0, 0, 10 * 1000).forEach(params => {
        expect(testedFn(params)).toBeFalsy()
      })

      generatePeriodicCheckParams(true, 0, 0, 10 * 1000).forEach(params => {
        if (params.chainStatus === SESSIONS_CHAIN_STATUSES.FINISHED) {
          expect(testedFn(params)).toBeFalsy()
        } else {
          expect(testedFn(params)).toBeTruthy()
        }
      })

      let paramsArr = generatePeriodicCheckParams(true, 1, 0, 10 * 1000)

      paramsArr.forEach((params, index) => {
        let momentWhenToPrepareToClose = params.maxDuration -
          2 * controllerInst.TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD

        if (params.chainStatus === SESSIONS_CHAIN_STATUSES.FINISHED) {
          expect(testedFn(params)).toBeFalsy()
        } else if (params.duration >= momentWhenToPrepareToClose) {
          expect(testedFn({
            ...params,
            tradeVolumeLeft: 2,
            shardVolume: 2
          })).toBeTruthy()
        } else {
          expect(testedFn(params)).toBeTruthy()
          expect(
            testedFn({
              ...params,
              entireChainPossibleOffersNo: 1,
              entireChainTradeRequestAnswersNo: 1,
              tradeVolumeLeft: 1,
              shardVolume: 2
            })
          ).toBeTruthy()
          expect(
            testedFn({
              ...params,
              tradeVolumeLeft: 0,
              shardVolume: 2,
              entireChainPossibleOffersNo: 1,
              entireChainTradeRequestAnswersNo: 1
            })
          ).toBeTruthy()
          expect(
            testedFn({...params, tradeVolumeLeft: 3, shardVolume: 2})
          ).toBeFalsy()
        }
      })
    })

    function generatePeriodicCheckParams (
      isIcebergTrade,
      entireChainPossibleOffersNo,
      entireChainTradeRequestAnswersNo,
      maxDuration
    ) {
      const inc = controllerInst.TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD
      let params = []

      for (let duration = 0; duration <= maxDuration; duration += inc) {
        Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
          params.push({
            isIcebergTrade,
            duration,
            maxDuration,
            entireChainPossibleOffersNo,
            entireChainTradeRequestAnswersNo,
            chainStatus: status,
            tradeVolumeLeft: 0,
            shardVolume: 1
          })
        })
      }

      return params
    }
  })
})
