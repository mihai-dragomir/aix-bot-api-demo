import DecisionController from '../DecisionController'
import marketPrices from '../../services/market-prices'

class OffersDecisionController extends DecisionController {
  constructor () {
    super()

    this.MIN_OFFERS_REQUIRED_FOR_FIRST_SPREAD = 1

    this.shouldSendFirstOffer = this.shouldSendFirstOffer.bind(this)
    this.shouldSendBetterOffer = this.shouldSendBetterOffer.bind(this)
    this.shouldTellOfferIsInvalid = this.shouldTellOfferIsInvalid.bind(this)
    this.getCurrentMarketPrice = this.getCurrentMarketPrice.bind(this)
    this.shouldSendBestSpread = this.shouldSendBestSpread.bind(this)

    this.MAX_DEVIATION = this.FIRST_OFFER_SPREAD_MAX_DEVIATION
    this.MIN_OFFERS_REQUIRED = this.MIN_OFFERS_REQUIRED_FOR_FIRST_SPREAD
    this.MAX_ACCEPTED_DEVIATION = this.MAX_ACCEPTED_OFFER_PRICE_DEVIATION_PERC
  }

  shouldSendFirstOffer (params) {
    const {
      transactionsSessionOffersNo,
      chainStatus,
      chainBestOffers,
      numberOfOffersSentToInitiator,

      duration
    } = params

    if (
      duration < this.FIRST_OFFERS_SEND_TIME_INTERVAL.START ||
      duration > this.FIRST_OFFERS_SEND_TIME_INTERVAL.END
    ) {
      return false
    }

    if (
      !this.ACTIVE_CHAIN_STATUSES.includes(chainStatus) ||
      this.MIN_OFFERS_REQUIRED <= 0 ||
      chainBestOffers == null ||
      typeof chainBestOffers !== 'object' ||
      typeof chainBestOffers.lowestOffer !== 'number' ||
      typeof chainBestOffers.highestBid !== 'number'
    ) {
      return false
    }

    return numberOfOffersSentToInitiator === 0 &&
      transactionsSessionOffersNo >= this.MIN_OFFERS_REQUIRED &&
      chainBestOffers.bidDeviationFromMarketPrice <= this.MAX_DEVIATION &&
      chainBestOffers.offerDeviationFromMarketPrice <= this.MAX_DEVIATION
  }

  shouldSendBetterOffer (params) {
    const {
      chainStatus,
      chainBestOffers,
      chainPreviousBestOffers,
      numberOfOffersSentToInitiator,

      priceToleranceLimitExists,
      isTradeOptionSpecified,

      duration
    } = params

    // a price limit was set to automatically execute transactions that fit
    if (
      this.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG && priceToleranceLimitExists
    ) {
      return false
    }

    // does this offer interrupt the initiator during a trade request dialog
    if (isTradeOptionSpecified) {
      return false
    }

    // is the right time period
    if (
      duration < this.BETTER_OFFERS_SEND_TIME_INTERVAL.START ||
      duration > this.BETTER_OFFERS_SEND_TIME_INTERVAL.END
    ) {
      return false
    }

    // validate params
    if (
      !this.ACTIVE_CHAIN_STATUSES.includes(chainStatus) ||
      chainBestOffers === null ||
      typeof chainBestOffers !== 'object' ||
      typeof chainBestOffers.lowestOffer !== 'number' ||
      typeof chainBestOffers.highestBid !== 'number' ||
      chainPreviousBestOffers === null ||
      typeof chainPreviousBestOffers !== 'object' ||
      typeof chainPreviousBestOffers.lowestOffer !== 'number' ||
      typeof chainPreviousBestOffers.highestBid !== 'number'
    ) {
      return false
    }

    return numberOfOffersSentToInitiator > 0 &&
      (
        chainBestOffers.lowestOffer < chainPreviousBestOffers.lowestOffer ||
        chainBestOffers.highestBid > chainPreviousBestOffers.highestBid
      )
  }

  shouldSendBestSpread (params) {
    let { numberOfOffersSentToInitiator, duration, chainBestOffers } = params

    return numberOfOffersSentToInitiator === 0 &&
      duration > this.BEST_OFFER_SEND_AFTER &&
      typeof chainBestOffers === 'object' &&
      typeof chainBestOffers.lowestOffer === 'number' &&
      typeof chainBestOffers.highestBid === 'number'
  }

  shouldTellOfferIsInvalid ({ bidPrice, offerPrice, financialInstrument }) {
    let marketPrice = this.getCurrentMarketPrice(financialInstrument)

    if (marketPrice === 0) {
      return false
    }
    if (typeof bidPrice !== 'number' || typeof offerPrice !== 'number') {
      return false
    }

    let bidDeviation = 1 - bidPrice / marketPrice
    let offerDeviation = offerPrice / marketPrice - 1

    return bidDeviation > this.MAX_ACCEPTED_DEVIATION ||
    offerDeviation > this.MAX_ACCEPTED_DEVIATION
  }

  getCurrentMarketPrice (financialInstrument) {
    return marketPrices.getCurrentPrice(financialInstrument)
  }
}

export default OffersDecisionController
