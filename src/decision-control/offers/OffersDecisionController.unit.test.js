import OffersDecisionController from './OffersDecisionController'
import {
  SESSIONS_CHAIN_STATUSES,
  ACTIVE_CHAIN_STATUSES
} from '../../constants'

describe('OffersDecisionController', () => {
  let controllerInst = null
  let testedFn = null
  let offer = null
  let betterOffer = null

  beforeEach(() => {
    controllerInst = new OffersDecisionController()
  })

  describe('instance', () => {
    test('should contain the needed constants', () => {
      expect(controllerInst).toMatchObject({
        MIN_OFFERS_REQUIRED_FOR_FIRST_SPREAD: expect.any(Number),
        ACTIVE_CHAIN_STATUSES: expect.any(Object),
        MAX_DEVIATION: expect.any(Number),
        MIN_OFFERS_REQUIRED: expect.any(Number),
        USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG: expect.any(Boolean),
        FIRST_OFFERS_SEND_TIME_INTERVAL: expect.objectContaining({
          START: expect.any(Number),
          END: expect.any(Number)
        }),
        BETTER_OFFERS_SEND_TIME_INTERVAL: expect.objectContaining({
          START: expect.any(Number),
          END: expect.any(Number)
        }),
        MAX_ACCEPTED_DEVIATION: expect.any(Number)
      })
    })
  })

  describe('shouldSendFirstOffer() function', () => {
    beforeEach(() => { testedFn = controllerInst.shouldSendFirstOffer })

    test('should return decision correctly', () => {
      offer = {
        highestBid: 5,
        highestBidTransactionIds: [],
        lowestOffer: 8,
        lowestOfferTransactionIds: [],
        bidDeviationFromMarketPrice: controllerInst.MAX_DEVIATION,
        offerDeviationFromMarketPrice: controllerInst.MAX_DEVIATION
      }
      let start = controllerInst.FIRST_OFFERS_SEND_TIME_INTERVAL.START
      let end = controllerInst.FIRST_OFFERS_SEND_TIME_INTERVAL.END

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        controllerInst.MIN_OFFERS_REQUIRED = 1

        if (ACTIVE_CHAIN_STATUSES.includes(status)) {
          expect(testedFn(getParams(0, 10, status, offer, end))).toBeTruthy()
          expect(
            testedFn(getParams(0, 10, status, offer, end + 1))
          ).toBeFalsy()
          expect(
            testedFn(getParams(0, 10, status, offer, start - 1))
          ).toBeFalsy()
        } else {
          expect(testedFn(getParams(0, 10, status, offer, end))).toBeFalsy()
          expect(
            testedFn(getParams(0, 10, status, offer, end + 1))
          ).toBeFalsy()
          expect(
            testedFn(getParams(0, 10, status, offer, start - 1))
          ).toBeFalsy()
        }

        expect(testedFn(getParams(1, 10, status, offer))).toBeFalsy()
        expect(testedFn(getParams(0, 10, status, {
          ...offer,
          bidDeviationFromMarketPrice: controllerInst.MAX_DEVIATION + 0.01,
          offerDeviationFromMarketPrice: controllerInst.MAX_DEVIATION + 0.01
        }))
        ).toBeFalsy()
        expect(testedFn(getParams(0, 10, status, { }))).toBeFalsy()

        controllerInst.MIN_OFFERS_REQUIRED = 11
        expect(testedFn(getParams(0, 10, status, offer))).toBeFalsy()

        controllerInst.MIN_OFFERS_REQUIRED = -100
        expect(testedFn(getParams(0, 10, status, offer))).toBeFalsy()

        expect(testedFn(getParams(0, 10, status, offer))).toBeFalsy()
      })
    })

    function getParams (
      numberOfOffersSentToInitiator,
      transactionsSessionOffersNo,
      chainStatus,
      chainBestOffers,
      duration = 0
    ) {
      return {
        numberOfOffersSentToInitiator,
        transactionsSessionOffersNo,
        chainStatus,
        chainBestOffers,
        duration
      }
    }
  })

  describe('shouldSendBetterOffer() function', () => {
    beforeEach(() => {
      testedFn = controllerInst.shouldSendBetterOffer
      offer = {
        highestBid: 5,
        highestBidTransactionIds: [],
        lowestOffer: 8,
        lowestOfferTransactionIds: [],
        bidDeviationFromMarketPrice: controllerInst.MAX_DEVIATION,
        offerDeviationFromMarketPrice: controllerInst.MAX_DEVIATION
      }
      betterOffer = {...offer, highestBid: 6, lowestOffer: 8}
    })

    test('should return decision correctly', () => {
      controllerInst.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = false

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        let expectation = expect(
          testedFn(getParams(1, status, betterOffer, offer))
        )

        if (ACTIVE_CHAIN_STATUSES.includes(status)) {
          expectation.toBeTruthy()
        } else {
          expectation.toBeFalsy()
        }

        expect(testedFn(getParams(0, status, betterOffer, offer))).toBeFalsy()
        expect(testedFn(getParams(1, status, offer, betterOffer))).toBeFalsy()
        expect(testedFn(getParams(0, status, offer, betterOffer))).toBeFalsy()
        expect(testedFn(getParams(1, status, betterOffer, {}))).toBeFalsy()
        expect(testedFn(getParams(1, status, {}, offer))).toBeFalsy()
        expect(testedFn(getParams(1, status, betterOffer, null))).toBeFalsy()
      })

      controllerInst.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = true

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        let expectation = expect(
          testedFn(getParams(1, status, betterOffer, offer, false))
        )

        if (ACTIVE_CHAIN_STATUSES.includes(status)) {
          expectation.toBeTruthy()
        } else {
          expectation.toBeFalsy()
        }

        expect(testedFn(getParams(1, status, betterOffer, offer, true)))
          .toBeFalsy()
      })
    })

    test('should decide true only in the interval set', () => {
      let params = getParams(
        1,
        SESSIONS_CHAIN_STATUSES.RUNNING,
        betterOffer,
        offer,
        false
      )

      let interval = controllerInst.BETTER_OFFERS_SEND_TIME_INTERVAL

      expect(testedFn({ ...params, duration: interval.START - 1 })).toBeFalsy()
      expect(testedFn({ ...params, duration: interval.START })).toBeTruthy()
      expect(testedFn({ ...params, duration: interval.START + 1 })).toBeTruthy()
      expect(testedFn({ ...params, duration: interval.END - 1 })).toBeTruthy()
      expect(testedFn({ ...params, duration: interval.END })).toBeTruthy()
      expect(testedFn({ ...params, duration: interval.END + 1 })).toBeFalsy()
    })

    function getParams (
      numberOfOffersSentToInitiator,
      chainStatus,
      chainBestOffers,
      chainPreviousBestOffers,
      priceToleranceLimitExists
    ) {
      return {
        numberOfOffersSentToInitiator,
        chainStatus,
        chainBestOffers,
        chainPreviousBestOffers,
        priceToleranceLimitExists,
        duration: controllerInst.BETTER_OFFERS_SEND_TIME_INTERVAL.START
      }
    }
  })

  describe('shouldTellOfferIsInvalid() function', () => {
    beforeEach(() => {
      controllerInst.getCurrentMarketPrice = jest.fn().mockImplementation(
        () => 6
      )
      testedFn = controllerInst.shouldTellOfferIsInvalid
      controllerInst.MAX_ACCEPTED_DEVIATION = 0.5
    })

    test('should answer true', () => {
      expect(testedFn(getParams(2, 10))).toBeTruthy()
      expect(testedFn(getParams(1, 100))).toBeTruthy()
      expect(testedFn(getParams(-10, 10))).toBeTruthy()
    })

    test('should answer false', () => {
      expect(testedFn(getParams(null, null))).toBeFalsy()
      expect(testedFn(getParams(20, null))).toBeFalsy()
      expect(testedFn(getParams(null, 20))).toBeFalsy()
      expect(testedFn(getParams(20, -20))).toBeFalsy()
      expect(testedFn(getParams(20, 0))).toBeFalsy()
      expect(testedFn(getParams('-10', '10'))).toBeFalsy()

      controllerInst.getCurrentMarketPrice = jest.fn().mockImplementation(
        () => 0
      )
      expect(testedFn(getParams(2, 10))).toBeFalsy()
    })

    function getParams (bidPrice, offerPrice) {
      return {bidPrice, offerPrice}
    }
  })

  describe('shouldSendBestSpread() function', () => {
    beforeEach(() => { testedFn = controllerInst.shouldSendBestSpread })

    test('should answer correctly when there is no need for this offer', () => {
      let params = {
        numberOfOffersSentToInitiator: 1,
        chainBestOffers: {
          lowestOffer: 12,
          highestBid: 10
        }
      }
      let t = controllerInst.BEST_OFFER_SEND_AFTER

      expect(testedFn({...params, duration: t - 60 * 1000})).toBeFalsy()
      expect(testedFn({...params, duration: t + 1})).toBeFalsy()
      expect(testedFn({...params, duration: t + 60 * 1000})).toBeFalsy()
    })

    test('should answer correctly when it is need for this offer', () => {
      let params = {
        numberOfOffersSentToInitiator: 0,
        chainBestOffers: {
          lowestOffer: 12,
          highestBid: 10
        }
      }
      let t = controllerInst.BEST_OFFER_SEND_AFTER

      expect(testedFn({...params, duration: t - 60 * 1000})).toBeFalsy()
      expect(testedFn({...params, duration: t + 1})).toBeTruthy()
      expect(testedFn({...params, duration: t + 60 * 1000})).toBeTruthy()
    })
  })
})
