import ChainsDecisionController from './ChainsDecisionController'
import { SESSIONS_CHAIN_STATUSES, ACTIVE_CHAIN_STATUSES } from
  '../../constants'

describe('ChainsDecisionController', () => {
  let controllerInst = null
  let testedFn = null

  beforeEach(() => { controllerInst = new ChainsDecisionController() })

  describe('instance', () => {
    test('should contain the needed constants', () => {
      expect(controllerInst).toMatchObject({
        MAX_INITIATOR_IDLE_TIME_AFTER_FIRST_OFFER: expect.any(Number),
        TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD: expect.any(Number),
        SESSIONS_CHAIN_STATUSES: expect.any(Object),
        ACTIVE_CHAIN_STATUSES: expect.any(Array)
      })
    })
  })

  describe('shouldMarkChainAsStalled() function', () => {
    beforeEach(() => {
      controllerInst.MAX_INITIATOR_IDLE_TIME_AFTER_FIRST_OFFER = 1000
      testedFn = controllerInst.shouldMarkChainAsStalled
    })

    test('should return decision correctly', () => {
      expect(testedFn(getParams(true, 0))).toBeFalsy()
      expect(testedFn(getParams(true, -1000))).toBeFalsy()
      expect(testedFn(getParams(true, 1000))).toBeFalsy()
      expect(testedFn(getParams(true, '1000'))).toBeFalsy()
      expect(testedFn(getParams(true, 2000))).toBeFalsy()
      expect(testedFn(getParams(true, undefined))).toBeFalsy()
      expect(testedFn(getParams(true, Infinity))).toBeFalsy()

      expect(testedFn(getParams(false, 0))).toBeFalsy()
      expect(testedFn(getParams(false, -1000))).toBeFalsy()
      expect(testedFn(getParams(false, 1000))).toBeFalsy()
      expect(testedFn(getParams(false, '1000'))).toBeFalsy()
      expect(testedFn(getParams(false, 2000))).toBeTruthy()
      expect(testedFn(getParams(false, undefined))).toBeFalsy()
      expect(testedFn(getParams(false, Infinity))).toBeTruthy()

      controllerInst.MAX_INITIATOR_IDLE_TIME_AFTER_FIRST_OFFER = undefined

      expect(testedFn(getParams(false, 2000))).toBeFalsy()
      expect(testedFn(getParams(false, Infinity))).toBeFalsy()
    })

    function getParams (tradeActionWasExpressed, timePassedSinceFirstOffer) {
      return {
        tradeActionWasExpressed,
        timePassedSinceFirstOffer
      }
    }
  })

  describe('shouldStopTransactionsChainByTimeout() function', () => {
    beforeEach(() => {
      testedFn = controllerInst.shouldStopTransactionsChainByTimeout
      controllerInst.MAX_INITIATOR_IDLE_TIME_AFTER_FIRST_OFFER = 30 * 1000
      controllerInst.TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD = 2 * 1000
    })

    test('should return decision correctly', () => {
      let twoMin = 2 * 60 * 1000

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        let expectation = expect(
          testedFn(getParams(twoMin - 1000, twoMin, status, 0))
        )

        if (ACTIVE_CHAIN_STATUSES.includes(status)) {
          expectation.toBeTruthy()
        } else {
          expectation.toBeFalsy()
        }
      })

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        expect(testedFn(getParams(0, twoMin, status, 0))).toBeFalsy()
      })

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        expect(testedFn(getParams(0, twoMin, status, 1))).toBeFalsy()
      })

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        expect(testedFn(getParams(0, twoMin, status, null))).toBeFalsy()
      })

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        let expectation = expect(
          testedFn(getParams(twoMin - 10 * 1000, twoMin, status, 0))
        )

        if (ACTIVE_CHAIN_STATUSES.includes(status)) {
          expectation.toBeTruthy()
        } else {
          expectation.toBeFalsy()
        }
      })

      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        expect(
          testedFn(getParams(twoMin - 10 * 1000, twoMin, status, 1))
        ).toBeFalsy()
      })
    })

    function getParams (
      duration,
      maxDuration,
      chainStatus,
      numberOfOffersSentToInitiator
    ) {
      return {
        duration,
        maxDuration,
        chainStatus,
        numberOfOffersSentToInitiator
      }
    }
  })

  describe('shouldCancelChain() function', () => {
    test('should return decision correctly', () => {
      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        let expectation = expect(
          controllerInst.shouldCancelChain({ chainStatus: status })
        )

        if (status === SESSIONS_CHAIN_STATUSES.CANCELLED) {
          expectation.toBeTruthy()
        } else {
          expectation.toBeFalsy()
        }
      })
    })
  })
})
