import TransactionsDecisionController from
  './transactions/TransactionsDecisionController'
import IcebergTradeDecisionController from
  './iceberg-trade/IcebergTradeDecisionController'
import OffersDecisionController from
  './offers/OffersDecisionController'
import ChainsDecisionController from './chain/ChainsDecisionController'
import StandardTradeDecisionController from
  './standard-trade/StandardTradeDecisionController'
import { DECISION_CONTROL_HOOKS } from '../constants'

let transactionsDecisionController = new TransactionsDecisionController()
let icebergTradeDecisionController = new IcebergTradeDecisionController()
let offersDecisionController = new OffersDecisionController()
let chainsDecisionController = new ChainsDecisionController()
let standardTradeDecisionController = new StandardTradeDecisionController()

export default {
  shouldAskForIcebergTrade:
    icebergTradeDecisionController.shouldAskForIcebergTrade,
  shouldExecuteTransactionAutomatically:
    transactionsDecisionController.shouldExecuteTransactionAutomatically,
  shouldDeclineTransactionAutomatically:
    transactionsDecisionController.shouldDeclineTransactionAutomatically,
  shouldTellOfferIsInvalid: offersDecisionController.shouldTellOfferIsInvalid,

  [DECISION_CONTROL_HOOKS.SHOULD_SEND_FIRST_OFFER]:
    offersDecisionController.shouldSendFirstOffer,
  [DECISION_CONTROL_HOOKS.SHOULD_SEND_BETTER_OFFER]:
    offersDecisionController.shouldSendBetterOffer,
  [DECISION_CONTROL_HOOKS.SHOULD_SEND_BEST_OFFER]:
    offersDecisionController.shouldSendBestSpread,
  [DECISION_CONTROL_HOOKS.SHOULD_STOP_TRANSACTIONS_SESSIONS_CHAIN_BY_TIMEOUT]:
    chainsDecisionController.shouldStopTransactionsChainByTimeout,
  [DECISION_CONTROL_HOOKS.SHOULD_MARK_CHAIN_AS_STALLED]:
    chainsDecisionController.shouldMarkChainAsStalled,
  [DECISION_CONTROL_HOOKS.SHOULD_FINALIZE_ICEBERG_TRADE]:
    icebergTradeDecisionController.shouldFinalizeIcebergTrade,
  [DECISION_CONTROL_HOOKS.SHOULD_FINALIZE_STANDARD_TRADE]:
    standardTradeDecisionController.shouldFinalizeStandardTrade,
  [DECISION_CONTROL_HOOKS.SHOULD_CANCEL_CHAIN]:
    chainsDecisionController.shouldCancelChain
}
