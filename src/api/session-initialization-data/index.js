import { Router } from 'express'
export SessionInitializationData, { schema } from './model'

const router = new Router()

export default router
