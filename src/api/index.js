import { Router } from 'express'
import botMessage from './bot-message'
import transaction from './transaction'
import bug from './bug'
import session from './session'
import trader from './trader'
import financialInstrument from './financial-instrument'
import transactionsSession from './transactions-session'
import completedTransaction from './completed-transaction'
import sessionInitializationData from './session-initialization-data'
import infoCard from './info-card'
import transactionsSessionChain from './transactions-session-chain'
import groupChat from './group-chat'
import auth from './auth'

const router = new Router()

router.use('/bot-messages', botMessage)
router.use('/transactions', transaction)
router.use('/bugs', bug)
router.use('/sessions', session)
router.use('/traders', trader)
router.use('/financial-instruments', financialInstrument)
router.use('/transactions-sessions', transactionsSession)
router.use('/completed-transactions', completedTransaction)
router.use('/session-initialization-data', sessionInitializationData)
router.use('/info-cards', infoCard)
router.use('/transactions-session-chains', transactionsSessionChain)
router.use('/group-chats', groupChat)
router.use('/auth', auth)

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */

export default router
