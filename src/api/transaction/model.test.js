import { Transaction } from '.'

let transaction

beforeEach(async () => {
  transaction = await Transaction.create({
    marketMakerSessionId: 'test',
    transactionsSessionId: 'test',
    traderId: 'test',
    marketMakerId: 'test',
    watsonContext: 'test',
    status: 'test',
    bidVolume: 1,
    offerVolume: 1,
    bidValue: 1,
    offerValue: 1,
    requestMonth: 'test',
    requestAction: 'test',
    closeAction: 'test',
    icebergTrade: true
  })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = transaction.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transaction.id)
    expect(view.marketMakerSessionId).toBe(transaction.marketMakerSessionId)
    expect(view.transactionsSessionId).toBe(transaction.transactionsSessionId)
    expect(view.traderId).toBe(transaction.traderId)
    expect(view.marketMakerId).toBe(transaction.marketMakerId)
    expect(view.watsonContext).toBe(transaction.watsonContext)
    expect(view.status).toBe(transaction.status)
    expect(view.bidVolume).toBe(transaction.bidVolume)
    expect(view.offerVolume).toBe(transaction.offerVolume)
    expect(view.bidValue).toBe(transaction.bidValue)
    expect(view.offerValue).toBe(transaction.offerValue)
    expect(view.closeAction).toBe(transaction.closeAction)
    expect(view.icebergTrade).toBe(transaction.icebergTrade)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = transaction.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transaction.id)
    expect(view.marketMakerSessionId).toBe(transaction.marketMakerSessionId)
    expect(view.transactionsSessionId).toBe(transaction.transactionsSessionId)
    expect(view.traderId).toBe(transaction.traderId)
    expect(view.marketMakerId).toBe(transaction.marketMakerId)
    expect(view.watsonContext).toBe(transaction.watsonContext)
    expect(view.status).toBe(transaction.status)
    expect(view.bidVolume).toBe(transaction.bidVolume)
    expect(view.offerVolume).toBe(transaction.offerVolume)
    expect(view.bidValue).toBe(transaction.bidValue)
    expect(view.offerValue).toBe(transaction.offerValue)
    expect(view.closeAction).toBe(transaction.closeAction)
    expect(view.icebergTrade).toBe(transaction.icebergTrade)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
