import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { password as passwordAuth, master, token } from '../../services/passport'
import { showMe, create, update, updatePassword, destroy } from './controller'
import { schema } from './model'

export Trader, { schema } from './model'

const router = new Router()
const {
  telegramId,
  password,
  firstName,
  lastName,
  financialInstrumentsInUse,
  companyName,
  traderType
} = schema.tree

/**
 * @api {get} /traders/me Retrieve current trader
 * @apiName RetrieveCurrentTrader
 * @apiGroup Trader
 * @apiPermission user
 * @apiParam {String} access_token Trader access_token.
 * @apiSuccess {Object} trader Trader's data.
 */
router.get('/me',
  token({ required: true }),
  showMe)

/**
 * @api {post} /traders Create trader
 * @apiName CreateTrader
 * @apiGroup Trader
 * @apiPermission master
 * @apiParam {String} access_token Master access_token.
 * @apiParam {String} telegramId Trader's telegramId.
 * @apiParam {String{6..}} password Trader's password.
 * @apiParam {String} [firstName] Trader's first name.
 * @apiParam {String} [lastName] Trader's last name.
 * @apiParam {Array} [financialInstrumentsInUse] Trader's financial instrument in use, ids.
 * @apiParam {String} [traderType] Trader's type.
 * @apiParam {String} [companyName] Trader's company name.
 * @apiSuccess (Sucess 201) {Object} trader Trader's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only.
 * @apiError 409 telegramId already registered.
 * @apiError 409 telegramId is invalid.
 */
router.post('/',
  master(),
  body({
    telegramId,
    password,
    firstName,
    lastName,
    financialInstrumentsInUse,
    traderType,
    companyName
  }),
  create)

/**
 * @api {put} /traders/:id Update trader
 * @apiName UpdateTrader
 * @apiGroup Trader
 * @apiPermission user
 * @apiParam {String} access_token Trader access_token.
 * @apiParam {String} [firstName] Trader's first name.
 * @apiParam {String} [lastName] Trader's last name.
 * @apiParam {String} [financialInstrumentsInUse] Trader's financial instrument in use, ids.
 * @apiParam {String} [traderType] Trader's type.
 * @apiParam {String} [companyName] Trader's company name.
 * @apiSuccess {Object} trader Trader's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Current trader or admin access only.
 * @apiError 404 Trader not found.
 */
router.put('/:id',
  token({ required: true }),
  body({
    firstName,
    lastName,
    financialInstrumentsInUse,
    traderType,
    companyName
  }),
  update
)

/**
 * @api {put} /traders/:id/password Update password
 * @apiName UpdatePassword
 * @apiGroup Trader
 * @apiPermission master
 * @apiHeader {String} Authorization Basic authorization with telegranId and password (eg: Basic NTQ5MDI4MTU5OjEyMzQ1Ng==).
 *  The header form must be: __"Basic ${credentials}"__.
 *  Credetials form must be: __convertToBase64("${telegramId}:${password}")__.
 * @apiParam {String{6..}} password Trader's new password.
 * @apiSuccess (Success 201) {Object} trader Trader's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Current trader access only.
 * @apiError 404 Trader not found.
 */
router.put('/:id/password',
  passwordAuth(),
  body({ password }),
  updatePassword)

/**
 * @api {delete} /traders/:id Delete trader
 * @apiName DeleteTrader
 * @apiGroup Trader
 * @apiPermission admin
 * @apiParam {String} access_token Trader access_token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 401 Admin access only.
 * @apiError 404 Trader not found.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
