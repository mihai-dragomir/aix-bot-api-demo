import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
import { BotMessage } from '.'
import traderDataMiddleware from '../../services/middlewares/trader-data'
import watsonWorkspaceDetectMiddleware from '../../services/middlewares/watson-workspace-detect'
import watsonCommunicationMiddleware from '../../services/middlewares/watson-communication'
import fsmMiddleware from '../../services/middlewares/fsm'
import watsonCardsMiddleware from '../../services/middlewares/watson-cards'
import { token } from '../../services/passport'

export BotMessage, { schema } from './model'

const router = new Router()
const { message, senderId } = schema.tree

/**
 * @api {post} /bot-messages Create bot message
 * @apiName CreateBotMessage
 * @apiGroup BotMessage
 * @apiPermission user
 * @apiHeader {String} Authorization Bearer token authorization header.
 * @apiParam message Bot message's message.
 * @apiParam senderId Bot message's senderId.
 * @apiSuccess {Object} botMessage Bot message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bot message not found.
 */
router.post('/',
  token({ required: true }),
  body({ message, senderId }),
  traderDataMiddleware,
  watsonWorkspaceDetectMiddleware,
  watsonCommunicationMiddleware,
  fsmMiddleware,
  watsonCardsMiddleware,
  create,
  function (req, res) {
    // Save outgoing messages
    if (req.session.outputMessage != null) {
      req.session.outputMessage.map((message) => {
        BotMessage.create({
          message: message,
          senderId: (req.session.trader != null) ? req.session.trader.telegramId : null,
          sessionId: req.session.id,
          isBotMessage: true
        })
          .catch((err) => {
            console.log('outputMessage save: Error!')
            console.log(err)
          })
      })
    }

    const r = {
      'outputMessage': req.session.outputMessage,
      'traderIntent': req.session.traderIntent,
      'marketMakerIntent': req.session.marketMakerIntent,
      'watsonUpdate': req.session.watson
    }

    res.send(r)
  }
)

/**
 * @api {get} /bot-messages Retrieve bot messages
 * @apiName RetrieveBotMessages
 * @apiGroup BotMessage
 * @apiUse listParams
 * @apiPermission user
 * @apiHeader {String} Authorization Bearer token authorization header.
 * @apiSuccess {Object[]} botMessages List of bot messages.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /bot-messages/:id Retrieve bot message
 * @apiName RetrieveBotMessage
 * @apiGroup BotMessage
 * @apiPermission user
 * @apiHeader {String} Authorization Bearer token authorization header.
 * @apiSuccess {Object} botMessage Bot message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bot message not found.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /bot-messages/:id Update bot message
 * @apiName UpdateBotMessage
 * @apiGroup BotMessage
 * @apiPermission user
 * @apiHeader {String} Authorization Bearer token authorization header.
 * @apiParam message Bot message's message.
 * @apiParam senderId Bot message's senderId.
 * @apiSuccess {Object} botMessage Bot message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bot message not found.
 */
router.put('/:id',
  token({ required: true }),
  body({ message, senderId }),
  update)

/**
 * @api {delete} /bot-messages/:id Delete bot message
 * @apiName DeleteBotMessage
 * @apiGroup BotMessage
 * @apiPermission user
 * @apiHeader {String} Authorization Bearer token authorization header.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Bot message not found.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
