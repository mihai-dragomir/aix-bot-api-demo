import { TransactionsSessionChain } from '.'

let transactionsSessionChain

beforeEach(async () => {
  transactionsSessionChain = await TransactionsSessionChain.create({
    transactionsSessionIds: ['test'],
    receiverSessionInitializationData: 'test',
    activeTransactionsSessionId: 'test',
    status: 'test',
    requestFiat: 'test',
    periodicCheckIntervalRef: 'test',
    financialInstrumentId: '123456789098765432123456',
    initiatorTraderSessionId: 'test',
    stalled: true,
    firstOfferSentAt: '2018-03-23T16:33:06.602Z',
    tradeActionWasExpressed: true,
    icebergTrade: true,
    soldShardTransactions: 'test',
    availableShards: 1,
    shardVolume: 1,
    totalShards: 1,
    priceToleranceLimit: 1,
    totalTradedVolume: 1,
    numberOfOffersSentToInitiator: 1,
    previousBestOffers: {},
    currentBestOffers: {}
  })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = transactionsSessionChain.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transactionsSessionChain.id)
    expect(view.transactionsSessionIds).toBe(transactionsSessionChain.transactionsSessionIds)
    expect(view.activeTransactionsSessionId).toBe(transactionsSessionChain.activeTransactionsSessionId)
    expect(view.receiverSessionInitializationData).toBe(transactionsSessionChain.receiverSessionInitializationData)
    expect(view.status).toBe(transactionsSessionChain.status)
    expect(view.requestFiat).toBe(transactionsSessionChain.requestFiat)
    expect(view.periodicCheckIntervalRef).toBe(transactionsSessionChain.periodicCheckIntervalRef)
    expect(view.financialInstrumentId).toBe(transactionsSessionChain.financialInstrumentId)
    expect(view.initiatorTraderSessionId).toBe(transactionsSessionChain.initiatorTraderSessionId)
    expect(view.stalled).toBe(transactionsSessionChain.stalled)
    expect(view.firstOfferSentAt).toBe(transactionsSessionChain.firstOfferSentAt)
    expect(view.tradeActionWasExpressed).toBe(transactionsSessionChain.tradeActionWasExpressed)
    expect(view.icebergTrade).toBe(transactionsSessionChain.icebergTrade)
    expect(view.availableShards).toBe(transactionsSessionChain.availableShards)
    expect(view.soldShardTransactions).toBe(transactionsSessionChain.soldShardTransactions)
    expect(view.shardVolume).toBe(transactionsSessionChain.shardVolume)
    expect(view.totalShards).toBe(transactionsSessionChain.totalShards)
    expect(view.priceToleranceLimit).toBe(transactionsSessionChain.priceToleranceLimit)
    expect(view.totalTradedVolume).toBe(transactionsSessionChain.totalTradedVolume)
    expect(view.numberOfOffersSentToInitiator).toBe(transactionsSessionChain.numberOfOffersSentToInitiator)
    expect(view.previousBestOffers).toBe(transactionsSessionChain.previousBestOffers)
    expect(view.currentBestOffers).toBe(transactionsSessionChain.currentBestOffers)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = transactionsSessionChain.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transactionsSessionChain.id)
    expect(view.transactionsSessionIds).toBe(transactionsSessionChain.transactionsSessionIds)
    expect(view.activeTransactionsSessionId).toBe(transactionsSessionChain.activeTransactionsSessionId)
    expect(view.receiverSessionInitializationData).toBe(transactionsSessionChain.receiverSessionInitializationData)
    expect(view.status).toBe(transactionsSessionChain.status)
    expect(view.requestFiat).toBe(transactionsSessionChain.requestFiat)
    expect(view.periodicCheckIntervalRef).toBe(transactionsSessionChain.periodicCheckIntervalRef)
    expect(view.financialInstrumentId).toBe(transactionsSessionChain.financialInstrumentId)
    expect(view.initiatorTraderSessionId).toBe(transactionsSessionChain.initiatorTraderSessionId)
    expect(view.stalled).toBe(transactionsSessionChain.stalled)
    expect(view.firstOfferSentAt).toBe(transactionsSessionChain.firstOfferSentAt)
    expect(view.tradeActionWasExpressed).toBe(transactionsSessionChain.tradeActionWasExpressed)
    expect(view.icebergTrade).toBe(transactionsSessionChain.icebergTrade)
    expect(view.availableShards).toBe(transactionsSessionChain.availableShards)
    expect(view.soldShardTransactions).toBe(transactionsSessionChain.soldShardTransactions)
    expect(view.shardVolume).toBe(transactionsSessionChain.shardVolume)
    expect(view.totalShards).toBe(transactionsSessionChain.totalShards)
    expect(view.priceToleranceLimit).toBe(transactionsSessionChain.priceToleranceLimit)
    expect(view.totalTradedVolume).toBe(transactionsSessionChain.totalTradedVolume)
    expect(view.numberOfOffersSentToInitiator).toBe(transactionsSessionChain.numberOfOffersSentToInitiator)
    expect(view.previousBestOffers).toBe(transactionsSessionChain.previousBestOffers)
    expect(view.currentBestOffers).toBe(transactionsSessionChain.currentBestOffers)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
