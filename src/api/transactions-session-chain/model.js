import mongoose, { Schema } from 'mongoose'

const transactionsSessionChainSchema = new Schema({
  transactionsSessionIds: {
    type: Array
  },
  activeTransactionsSessionId: {
    type: String
  },
  status: {
    type: String
  },
  receiverSessionInitializationData: {
    type: Object
  },
  initiatorTraderId: {
    type: String
  },
  askedBid: {
    type: Number
  },
  askedOffer: {
    type: Number
  },
  financialInstrumentId: {
    type: Schema.ObjectId
  },
  initiatorTraderSessionId: {
    type: String // a hex string
  },
  requestFiat: {
    type: String
  },
  stalled: {
    type: Boolean
  },
  firstOfferSentAt: {
    type: Date
  },
  tradeActionWasExpressed: {
    type: Boolean
  },
  tradeOption: {
    type: String
  },
  icebergTrade: {
    type: Boolean
  },
  totalShards: {
    type: Number
  },
  availableShards: {
    type: Number
  },
  soldShardTransactions: {
    type: Array
  },
  shardVolume: {
    type: Number
  },
  priceToleranceLimit: {
    type: Number
  },
  totalTradedVolume: {
    type: Number
  },
  tradeVolumeLeft: {
    type: Number
  },
  numberOfOffersSentToInitiator: {
    type: Number
  },
  tradeOptionSpecified: {
    type: Boolean
  },
  previousBestOffers: {
    type: Object
  },
  currentBestOffers: {
    type: Object
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  },
  usePushEach: true
})

transactionsSessionChainSchema.methods = {
  getId () {
    return this.id
  },
  getTransactionsSessionIds () {
    return this.transactionsSessionIds || []
  },
  getTradeActionWasExpressed () {
    return this.tradeActionWasExpressed
  },
  getTradeOption () {
    return this.tradeOption
  },
  getAskedOffer () {
    return this.askedOffer
  },
  setAskedBid (value) {
    this.askedBid = value
  },
  getAskedBid () {
    return this.askedBid
  },
  getFirstOfferSentAt () {
    return this.firstOfferSentAt
  },
  getFinancialInstrumentId () {
    return this.financialInstrumentId
  },
  getPriceToleranceLimit () {
    return this.priceToleranceLimit
  },
  getAvailableShards () {
    return this.availableShards
  },
  getSoldShardTransactionIds () {
    return this.soldShardTransactions
  },
  getShardVolume () {
    return this.shardVolume
  },
  getTotalTradedVolume () {
    return this.totalTradedVolume
  },
  getTotalShards () {
    return this.totalShards
  },
  getActiveTransactionsSessionId () {
    return this.activeTransactionsSessionId
  },
  getStatus () {
    return this.status
  },
  getInitiatorId () {
    return this.initiatorTraderId
  },
  getNumberOfOffersSentToInitiator () {
    return this.numberOfOffersSentToInitiator || 0
  },
  getPreviousBestOffers () {
    return this.previousBestOffers
  },
  getInitiatorTraderSession () {
    return this.initiatorTraderSessionId
  },
  getReceiverSessionInitializationData () {
    return this.receiverSessionInitializationData
  },
  getCurrentBestOffers () {
    return this.currentBestOffers
  },
  getTradeVolumeLeft () {
    return this.tradeVolumeLeft
  },

  isStalled () {
    return this.stalled || false
  },
  isIcebergTrade () {
    return this.icebergTrade || false
  },
  isTradeOptionSpecified () {
    return this.tradeOptionSpecified || false
  },
  wasTradeActionExpresses () {
    return this.tradeActionWasExpressed || false
  },
  isTradeVolumeLeft () {
    return this.tradeVolumeLeft > 0
  },

  setStalled (boolValue) {
    this.stalled = boolValue
  },
  setTradeActionWasExpressed (boolValue) {
    this.tradeActionWasExpressed = boolValue
  },
  setTradeOption (traderOption) {
    this.tradeOption = traderOption
  },
  setAskedOffer (value) {
    this.askedOffer = value
  },
  setFirstOfferSentAt (value) {
    this.firstOfferSentAt = value
  },
  setPriceToleranceLimit (value) {
    this.priceToleranceLimit = value
  },
  setAvailableShards (newValue) {
    this.availableShards = newValue
  },
  setShardVolume (volume) {
    this.shardVolume = volume
  },
  setTotalTradedVolume (volume) {
    this.totalTradedVolume = volume
  },
  setStatus (newStatus) {
    this.status = newStatus
  },
  setActiveTransactionsSessionId (newId) {
    this.activeTransactionsSessionId = newId
  },
  setNumberOfOffersSentToInitiator (newValue) {
    this.numberOfOffersSentToInitiator = newValue
  },
  setTradeOptionSpecified (newValue) {
    this.tradeOptionSpecified = newValue
  },
  setPreviousBestOffers (newObj) {
    this.previousBestOffers = newObj
  },
  setReceiverSessionInitializationData (newObj) {
    this.receiverSessionInitializationData = newObj
  },
  setCurrentBestOffers (newObj) {
    this.currentBestOffers = newObj
  },
  setTradeVolumeLeft (newValue) {
    this.tradeVolumeLeft = newValue
  },

  addSoldShardTransactionId (transactionId) {
    let arr = this.soldShardTransactions || []

    arr.push(transactionId)

    this.soldShardTransactions = arr
  },

  view (full) {
    const view = {
      // simple view
      id: this.id,
      transactionsSessionIds: this.transactionsSessionIds,
      activeTransactionsSessionId: this.activeTransactionsSessionId,
      receiverSessionInitializationData: this.receiverSessionInitializationData,
      status: this.status,
      initiatorTraderId: this.initiatorTraderId,
      financialInstrumentId: this.financialInstrumentId,
      askedBid: this.askedBid,
      askedOffer: this.askedOffer,
      initiatorTraderSessionId: this.initiatorTraderSessionId,
      requestFiat: this.requestFiat,
      stalled: this.stalled,
      firstOfferSentAt: this.firstOfferSentAt,
      tradeActionWasExpressed: this.tradeActionWasExpressed,
      tradeOption: this.traderOption,
      icebergTrade: this.icebergTrade,
      totalShards: this.totalShards,
      availableShards: this.availableShards,
      soldShardTransactions: this.soldShardTransactions,
      shardVolume: this.shardVolume,
      priceToleranceLimit: this.priceToleranceLimit,
      numberOfOffersSentToInitiator: this.numberOfOffersSentToInitiator,
      tradeOptionSpecified: this.tradeOptionSpecified,
      totalTradedVolume: this.totalTradedVolume,
      previousBestOffers: this.previousBestOffers,
      currentBestOffers: this.currentBestOffers,
      tradeVolumeLeft: this.tradeVolumeLeft,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('TransactionsSessionChain', transactionsSessionChainSchema)

export const schema = model.schema
export default model
