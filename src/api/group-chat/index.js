/* eslint camelcase: 0 */
import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { create, createInfoCard } from './controller'
import { schema } from './model'
import { master } from '../../services/passport'

export GroupChat, { schema } from './model'

const router = new Router()
const { message, group_name } = schema.tree

/**
 * @api {post} /group-chats Create group chat
 * @apiName CreateGroupChat
 * @apiGroup GroupChat
 * @apiPermission master
 * @apiParam message Group chat's message.
 * @apiParam group_name Group chat's group_name.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Object} groupChat Group chat's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Group chat not found.
 */
router.post('/',
  master(),
  body({ message, group_name }),
  create)

/**
 * @api {post} /group-chats/info-card Create group chat price info card
 * @apiName CreateGroupChatInfoCard
 * @apiGroup GroupChat
 * @apiPermission master
 * @apiParam message Group chat's message.
 * @apiParam group_name Group chat's group_name.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Object} groupChat Group chat's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Group chat not found.
 */
router.post('/info-card',
  master(),
  body({ message, group_name }),
  createInfoCard)

export default router
