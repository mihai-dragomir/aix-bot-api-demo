import request from 'supertest'
import { apiRoot, masterKey } from '../../config'
import express from '../../services/express'
import routes, { GroupChat } from '.'

const app = () => express(apiRoot, routes)

beforeEach(async () => {
  await GroupChat.create({})
})

test('POST /group-chats 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ message: 'test', group_name: 'test' })
  expect(status).toBe(401)
})

test('POST /group-chats 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, message: 'test', group_name: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.message).toEqual('test')
  expect(body.group_name).toEqual('test')
})
