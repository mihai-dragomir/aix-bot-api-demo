import { success, notFound } from '../../services/response/'
import { Bug } from '.'
import { createServiceDeskIssue, addJsonAttachmentToServiceDeskIssue } from '../../services/external/service-desk/ServiceDeskClient'
import { BotMessage } from '../bot-message'

export const create = async ({ bodymen: { body }, session }, res, next) => {
  body.session = session
  if (session.watson != null && session.watson.context != null) {
    body.watsonContext = session.watson.context
  }

  const result = await Promise.all([
    BotMessage.find({ sessionId: body.session.id }),
    Bug.create(body)
  ])
  const [botMessages, createdBug] = result
  let {
    firstName,
    lastName,
    email,
    telegramId
  } = body.session.trader

  try {
    if (process.env.SERVICE_DESK_BUG_REPORTING_ENABLED === 'true') {
      let issue = await createServiceDeskIssue(firstName, lastName, email, telegramId, body.bug_description)
      if (issue && body.watsonContext) {
        await addJsonAttachmentToServiceDeskIssue(issue, body.watsonContext, 'watsonContext')
      }
      if (issue && botMessages) {
        await addJsonAttachmentToServiceDeskIssue(issue, botMessages, 'botConversation')
      }
    }
    return success(res, 201)(createdBug.view(true))
  } catch (error) {
    return next(error)
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Bug.count(query)
    .then(count => Bug.find(query, select, cursor)
      .then((bugs) => ({
        count,
        rows: bugs.map((bug) => bug.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Bug.findById(params.id)
    .then(notFound(res))
    .then((bug) => bug ? bug.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Bug.findById(params.id)
    .then(notFound(res))
    .then((bug) => bug ? Object.assign(bug, body).save() : null)
    .then((bug) => bug ? bug.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Bug.findById(params.id)
    .then(notFound(res))
    .then((bug) => bug ? bug.remove() : null)
    .then(success(res, 204))
    .catch(next)
