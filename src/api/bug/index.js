/* eslint camelcase: 0 */
import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
import { master, token } from '../../services/passport'
import asyncHandler from 'express-async-handler'

export Bug, { schema } from './model'

const router = new Router()
const { bug_description, conversation_id, user_telegram_id } = schema.tree

/**
 * @api {post} /bugs Create bug
 * @apiName CreateBug
 * @apiGroup Bug
 * @apiPermission master
 * @apiParam bug_description Bug's bug_description.
 * @apiParam conversation_id Bug's conversation_id.
 * @apiParam user_telegram_id Bug's user_telegram_id.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Object} bug Bug's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only or invalid token.
 * @apiError 404 Bug not found.
 */
router.post('/',
  token({ required: true }),
  body({ bug_description, conversation_id, user_telegram_id }),
  asyncHandler(create))

/**
 * @api {get} /bugs Retrieve bugs
 * @apiName RetrieveBugs
 * @apiGroup Bug
 * @apiPermission master
 * @apiUse listParams
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Number} count Total amount of bugs.
 * @apiSuccess {Object[]} rows List of bugs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only or invalid token.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /bugs/:id Retrieve bug
 * @apiName RetrieveBug
 * @apiGroup Bug
 * @apiPermission master
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Object} bug Bug's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only or invalid token.
 * @apiError 404 Bug not found.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /bugs/:id Update bug
 * @apiName UpdateBug
 * @apiGroup Bug
 * @apiPermission master
 * @apiParam bug_description Bug's bug_description.
 * @apiParam conversation_id Bug's conversation_id.
 * @apiParam user_telegram_id Bug's user_telegram_id.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Object} bug Bug's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only or invalid token.
 * @apiError 404 Bug not found.
 */
router.put('/:id',
  master(),
  body({ bug_description, conversation_id, user_telegram_id }),
  update)

/**
 * @api {delete} /bugs/:id Delete bug
 * @apiName DeleteBug
 * @apiGroup Bug
 * @apiPermission master
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 401 Master access only or invalid token.
 * @apiError 404 Bug not found.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
