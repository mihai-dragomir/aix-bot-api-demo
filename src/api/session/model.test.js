import { Session } from '.'

let session

beforeEach(async () => {
  session = await Session.create({ _id: 'test', session: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = session.view()

    expect(typeof view).toBe('object')
    expect(view.id).toBe(session.id)
    expect(view.session).toBe(session.session)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = session.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(session.id)
    expect(view.session).toBe(session.session)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
