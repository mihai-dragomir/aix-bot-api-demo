import mongoose, { Schema } from 'mongoose'

const sessionSchema = new Schema({
  _id: { // specifically added to allow sessions query by hex id
    type: String
  },
  session: {
    type: Object
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

sessionSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      session: this.session,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Session', sessionSchema)

export const schema = model.schema
export default model
