import pug from 'pug'
import phantom from 'phantom'
import aws from 'aws-sdk'
import fs from 'fs'
import { success } from '../../services/response/'
import InfoCard from './model'
import config from '../../config'

aws.config.update(config.s3)
let s3 = new aws.S3()
let phantomInstance = null
let compiledTemplate = pug.compileFile(
  'src/api/info-card/template/info-card.pug'
)
const clipRect = {
  top: 5,
  left: 5,
  width: 605,
  height: 449
}

const MAX_WAIT_TIME_FOR_CARD = 3 * 1000
let fileCache = {}
const basePath = `${__dirname}/../../../cards-host/info-cards`
// if (!fs.existsSync(basePath)) {
//   throw new Error(`The path ${basePath} does not exist!`)
// }
// fs.access(basePath, fs.constants.W_OK, function (err) {
//   if (err) {
//     throw new Error('You do not have the rights to write on this folder')
//   }
// })

const fileToBase64 = (file) => {
  if (fileCache[file]) {
    return fileCache[file]
  }
  const fileContent = fs.readFileSync(file)
  fileCache[file] = Buffer.from(fileContent).toString('base64')
  return fileCache[file]
}

const generateCardImage = content => new Promise(async (resolve, reject) => {
  const filePath = `${basePath}/${Date.now()}.png`
  const templateVars = {
    ...content,
    cardBackgroundImageUrl: `data:image/png;base64,${fileToBase64(`${__dirname}/template/background.png`)}`,
    robotoUrl: `data:font/truetype;charset=utf-8;base64,${fileToBase64(`${__dirname}/template/Roboto-Regular.ttf`)}`,
    robotoThinUrl: `data:font/truetype;charset=utf-8;base64,${fileToBase64(`${__dirname}/template/Roboto-Thin.ttf`)}`,
    robotoBoldUrl: `data:font/truetype;charset=utf-8;base64,${fileToBase64(`${__dirname}/template/Roboto-Bold.ttf`)}`,
    robotoLightUrl: `data:font/truetype;charset=utf-8;base64,${fileToBase64(`${__dirname}/template/Roboto-Light.ttf`)}`
  }

  if (phantomInstance === null) {
    phantomInstance = await phantom.create()
  }
  const page = await phantomInstance.createPage()
  page.property('viewportSize', { width: 600, height: 444 })
  page.property('clipRect', clipRect)
  page.on('onResourceError', function (resourceError) {
    console.log(`Unable to load resource (#${resourceError.id} URL:${resourceError.url})`)
    console.log(`Error code: ${resourceError.errorCode} Description: ${resourceError.errorString}`)
  })
  let timeoutObject = setTimeout(() => {
    page.close()
    return reject(new Error(`PhantomJS failed to generate a card in less than ${MAX_WAIT_TIME_FOR_CARD}  ms`))
  }, MAX_WAIT_TIME_FOR_CARD)
  page.on('onLoadFinished', async status => {
    await page.render(filePath)
    clearTimeout(timeoutObject)
    page.close()
    resolve(filePath)
  })
  page.property('content', compiledTemplate(templateVars))
})

export const create = ({ bodymen: { body } }, res, next) => {
  return generateCardImage(body.content)
    .then(filePath => {
      body.filePath = filePath
      return uploadImageToS3(filePath)
    })
    .then(url => {
      body.url = url
      InfoCard.create(body)
        .then((infoCard) => infoCard.view(true))
        .then(success(res, 201))
        .catch(next)
    })
    .catch(next)
}

const uploadImageToS3 = pathToImage => new Promise((resolve, reject) => {
  fs.readFile(pathToImage, function (err, data) {
    if (err) {
      console.log('fs.ReadFile returned ERR:', err)

      return reject(new Error('File read failed.'))
    }

    let params = {
      Body: data,
      ACL: 'public-read',
      Bucket: config.s3.bucket,
      Key: pathToImage,
      ContentType: 'png'
    }

    s3.upload(params, function (err, data) {
      if (err) {
        console.log('s3.upload returned ERR:', err)
        return reject(new Error('File upload failed.'))
      }
      let fileUrl = data.Location

      resolve(fileUrl)
      // delete image from disk
      fs.unlink(pathToImage)
    })
  })
})
