import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { create } from './controller'
import { schema } from './model'
import { master } from '../../services/passport'

export InfoCard, { schema } from './model'

const router = new Router()
const { label, content } = schema.tree

/**
 * @api {post} /info-cards Create info card
 * @apiName CreateInfoCard
 * @apiGroup InfoCard
 * @apiPermission master
 * @apiParam label Info card's label.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess {Object} infoCard Info card's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Info card not found.
 */
router.post('/',
  master(),
  body({ label, content }),
  create)

export default router
