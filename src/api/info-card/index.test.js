import request from 'supertest'
import { apiRoot, masterKey } from '../../config'
import express from '../../services/express'
import routes, { InfoCard } from '.'

const app = () => express(apiRoot, routes)

beforeEach(async () => {
  await InfoCard.create({})
})

test('POST /info-cards 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ label: 'test', content: 'test' })
  expect(status).toBe(401)
})

test('POST /info-cards 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, label: 'test', content: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.label).toEqual('test')
  expect(body.content).toEqual('test')
})
